package spj.database.korisnikproracuna;

import spj.database.SpjDomainMap;
import spj.database.SpjDomainModelEntity;
import spj.database.SpjDomainModelMapper;
import spj.database.glava.GlavaEntity;
import spj.shared.domain.Glava;
import spj.shared.domain.KorisnikProracuna;
import spj.shared.domain.SpjDomainModel;
import spj.shared.util.SpjAssert;
import spj.util.SpjCastTool;

public class KorisnikProracunaMapper extends SpjDomainModelMapper
{
    @Override
    public <T extends SpjDomainModelEntity> T createEntity(SpjDomainModel spjDomainModel)
    {
        SpjAssert.isNotNull(spjDomainModel, "spjDomainModel");

        KorisnikProracuna korisnikProracuna = SpjCastTool.cast(spjDomainModel);
        GlavaEntity glavaEntity = SpjDomainMap.getMap(Glava.class).getMapper().createEntity(korisnikProracuna.getGlava());

        KorisnikProracunaEntity korisnikProracunaEntity = new KorisnikProracunaEntity();
        korisnikProracunaEntity.setNaziv(korisnikProracuna.getNaziv());
        korisnikProracunaEntity.setId(korisnikProracuna.getId());
        korisnikProracunaEntity.setGlavaEntity(glavaEntity);

        return SpjCastTool.cast(korisnikProracunaEntity);
    }

    @Override
    public <T extends SpjDomainModel> T createDomainModel(SpjDomainModelEntity spjDomainModelEntity)
    {
        SpjAssert.isNotNull(spjDomainModelEntity, "spjDomainModelEntity");

        KorisnikProracunaEntity korisnikProracunaEntity = SpjCastTool.cast(spjDomainModelEntity);

        Glava glava = SpjDomainMap.getMap(Glava.class).getMapper().createDomainModel(korisnikProracunaEntity.getGlavaEntity());

        return SpjCastTool.cast(KorisnikProracuna.builder()
                .id(korisnikProracunaEntity.getId().toString())
                .naziv(korisnikProracunaEntity.getNaziv())
                .glava(glava)
                .build());
    }
}
