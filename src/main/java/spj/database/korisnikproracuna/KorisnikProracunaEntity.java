package spj.database.korisnikproracuna;

import spj.database.SpjDomainModelEntity;
import spj.database.glava.GlavaEntity;

import javax.persistence.*;

@Entity
public class KorisnikProracunaEntity implements SpjDomainModelEntity
{
    @Id
    private Long id;

    private String naziv;

    @ManyToOne( optional = false)
    private GlavaEntity glavaEntity;

    public Long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getNaziv()
    {
        return naziv;
    }

    public void setNaziv(String naziv)
    {
        this.naziv = naziv;
    }

    public GlavaEntity getGlavaEntity()
    {
        return this.glavaEntity;
    }

    public void setGlavaEntity(GlavaEntity glavaEntity)
    {
        this.glavaEntity = glavaEntity;
    }
}
