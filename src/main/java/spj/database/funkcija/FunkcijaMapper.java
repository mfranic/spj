package spj.database.funkcija;


import spj.database.SpjDomainModelEntity;
import spj.database.SpjDomainModelMapper;
import spj.shared.domain.Funkcija;
import spj.shared.domain.SpjDomainModel;
import spj.shared.util.SpjAssert;
import spj.util.SpjCastTool;

public class FunkcijaMapper extends SpjDomainModelMapper
{
    @Override
    public <T extends SpjDomainModel> T createDomainModel(SpjDomainModelEntity spjDomainModelEntity)
    {
        SpjAssert.isNotNull(spjDomainModelEntity, "funkcijaEntity");
        SpjAssert.isOfType(spjDomainModelEntity, FunkcijaEntity.class, "funkcijaEntity");

        FunkcijaEntity funkcijaEntity = SpjCastTool.cast(spjDomainModelEntity);

        return SpjCastTool.cast(Funkcija.builder()
                .id(funkcijaEntity.getId().toString())
                .naziv(funkcijaEntity.getNaziv())
                .build());
    }

    @Override
    public <T extends SpjDomainModelEntity> T createEntity(SpjDomainModel spjDomainModel)
    {
        SpjAssert.isNotNull(spjDomainModel, "funkcija");
        SpjAssert.isOfType(spjDomainModel, Funkcija.class, "funkcija");

        Funkcija funkcija = SpjCastTool.cast(spjDomainModel);

        FunkcijaEntity funkcijaEntity = new FunkcijaEntity();
        funkcijaEntity.setId(funkcija.getId());
        funkcijaEntity.setNaziv(funkcija.getNaziv());

        return SpjCastTool.cast(funkcijaEntity);
    }
}
