package spj.database.izvod;

import spj.database.SpjDomainModelEntity;
import spj.database.konto.KontoEntity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class IzvodEntity implements SpjDomainModelEntity {
    @Id
    private Long id;
    private Date datum;
    private Long iznos;
    private String opis;
    private Integer brojIzvoda;
    private Boolean zakljucen;

    @ManyToOne(optional = false)
    private KontoEntity kontoEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Long getIznos() {
        return iznos;
    }

    public void setIznos(Long iznos) {
        this.iznos = iznos;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public KontoEntity getKontoEntity() {
        return kontoEntity;
    }

    public void setKontoEntity(KontoEntity kontoEntity) {
        this.kontoEntity = kontoEntity;
    }

    public Integer getBrojIzvoda()
    {
        return brojIzvoda;
    }

    public void setBrojIzvoda(Integer brojIzvoda)
    {
        this.brojIzvoda = brojIzvoda;
    }

    public Boolean getZakljucen()
    {
        return zakljucen;
    }

    public void setZakljucen(Boolean zakljucen)
    {
        this.zakljucen = zakljucen;
    }
}