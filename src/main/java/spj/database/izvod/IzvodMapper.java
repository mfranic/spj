package spj.database.izvod;

import spj.database.SpjDomainMap;
import spj.database.SpjDomainModelEntity;
import spj.database.SpjDomainModelMapper;
import spj.database.konto.KontoEntity;
import spj.shared.domain.Izvod;
import spj.shared.domain.Konto;
import spj.shared.domain.SpjDomainModel;
import spj.shared.util.SpjAssert;
import spj.util.SpjCastTool;

public class IzvodMapper extends SpjDomainModelMapper
{

    @Override
    public <T extends SpjDomainModel> T createDomainModel(SpjDomainModelEntity spjDomainModelEntity)
    {
        SpjAssert.isNotNull(spjDomainModelEntity, "spjDomainModelEntity");

        IzvodEntity izvodEntity = SpjCastTool.cast(spjDomainModelEntity);

        Konto konto = SpjDomainMap.KONTO.getMapper().createDomainModel(izvodEntity.getKontoEntity());

        return SpjCastTool.cast(Izvod.builder()
                .id(izvodEntity.getId().toString())
                .datum(izvodEntity.getDatum())
                .iznos(izvodEntity.getIznos())
                .opis(izvodEntity.getOpis())
                .konto(konto)
                .brojIzvoda(izvodEntity.getBrojIzvoda())
                .zakljucen(izvodEntity.getZakljucen())
                .build()
        );
    }

    @Override
    public <T extends SpjDomainModelEntity> T createEntity(SpjDomainModel spjDomainModel)
    {
        SpjAssert.isNotNull(spjDomainModel, "spjDomainModel");

        Izvod izvod = SpjCastTool.cast(spjDomainModel);

        KontoEntity kontoEntity = SpjDomainMap.KONTO.getMapper().createEntity(izvod.getKonto());

        IzvodEntity izvodEntity = new IzvodEntity();
        izvodEntity.setId(izvod.getId());
        izvodEntity.setDatum(izvod.getDatum());
        izvodEntity.setIznos(izvod.getIznos());
        izvodEntity.setOpis(izvod.getOpis());
        izvodEntity.setKontoEntity(kontoEntity);
        izvodEntity.setBrojIzvoda(izvod.getBrojIzvoda());
        izvodEntity.setZakljucen(izvod.getZakljucen());

        return SpjCastTool.cast(izvodEntity);
    }
}
