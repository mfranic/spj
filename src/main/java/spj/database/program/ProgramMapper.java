package spj.database.program;

import spj.database.SpjDomainMap;
import spj.database.SpjDomainModelEntity;
import spj.database.SpjDomainModelMapper;
import spj.database.glavniprogram.GlavniProgramEntity;
import spj.shared.domain.GlavniProgram;
import spj.shared.domain.Program;
import spj.shared.domain.SpjDomainModel;
import spj.shared.util.SpjAssert;
import spj.shared.util.SpjEvaluate;
import spj.util.SpjCastTool;

public class ProgramMapper extends SpjDomainModelMapper
{
    @Override
    public <T extends SpjDomainModel> T createDomainModel(SpjDomainModelEntity spjDomainModelEntity)
    {
        SpjAssert.isNotNull(spjDomainModelEntity, "programEntity");
        SpjAssert.isOfType(spjDomainModelEntity, ProgramEntity.class, "programEntity");

        ProgramEntity programEntity = SpjCastTool.cast(spjDomainModelEntity);

        GlavniProgram glavniProgram = SpjDomainMap.GLAVNI_PROGRAM.getMapper().createDomainModel(programEntity.getGlavniProgramEntity());

        Program.Builder programBuilder = Program.builder()
                .id(programEntity.getId().toString())
                .naziv(programEntity.getNaziv())
                .zakonskaOsnova(programEntity.getZakonskaOsnova())
                .glavniProgram(glavniProgram)
                .opis(programEntity.getOpis())
                .opciCilj(programEntity.getOpciCilj())
                .pokazateljUspijeha(programEntity.getPokazateljUspijeha());

        if(SpjEvaluate.isNotNull(programEntity.getBrojZaposlenih()))
        {
            programBuilder.brojZaposlenih(programEntity.getBrojZaposlenih().toString());
        }

        return SpjCastTool.cast(programBuilder.build());
    }

    @Override
    public <T extends SpjDomainModelEntity> T createEntity(SpjDomainModel spjDomainModel)
    {
        SpjAssert.isNotNull(spjDomainModel, "program");
        SpjAssert.isOfType(spjDomainModel, Program.class, "program");

        Program program = SpjCastTool.cast(spjDomainModel);

        GlavniProgramEntity glavniProgramEntity = SpjDomainMap.GLAVNI_PROGRAM.getMapper().createEntity(program.getGlavniProgram());

        ProgramEntity programEntity = new ProgramEntity();
        programEntity.setId(program.getId());
        programEntity.setNaziv(program.getNaziv());
        programEntity.setZakonskaOsnova(program.getZakonskaOsnova());
        programEntity.setBrojZaposlenih(program.getBrojZaposlenih());
        programEntity.setGlavniProgramEntity(glavniProgramEntity);
        programEntity.setOpis(program.getOpis());
        programEntity.setOpciCilj(program.getOpciCilj());
        programEntity.setPokazateljUspijeha(program.getPokazateljUspijeha());

        return SpjCastTool.cast(programEntity);
    }
}
