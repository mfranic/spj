package spj.database.program;

import spj.database.SpjDomainModelEntity;
import spj.database.glavniprogram.GlavniProgramEntity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class ProgramEntity implements SpjDomainModelEntity
{

    @Id
    private Long id;
    private String naziv;
    private String zakonskaOsnova;
    private String opis;
    private String opciCilj;
    @ManyToOne(optional = false)
    private GlavniProgramEntity glavniProgramEntity;
    private Integer brojZaposlenih;
    private String pokazateljUspijeha;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNaziv()
    {
        return naziv;
    }

    public void setNaziv(String naziv)
    {
        this.naziv = naziv;
    }

    public String getZakonskaOsnova()
    {
        return zakonskaOsnova;
    }

    public void setZakonskaOsnova(String zakonskaOsnova)
    {
        this.zakonskaOsnova = zakonskaOsnova;
    }

    public String getOpis()
    {
        return opis;
    }

    public void setOpis(String opis)
    {
        this.opis = opis;
    }

    public String getOpciCilj()
    {
        return opciCilj;
    }

    public void setOpciCilj(String opciCilj)
    {
        this.opciCilj = opciCilj;
    }

    public GlavniProgramEntity getGlavniProgramEntity()
    {
        return glavniProgramEntity;
    }

    public void setGlavniProgramEntity(GlavniProgramEntity glavniProgramEntity)
    {
        this.glavniProgramEntity = glavniProgramEntity;
    }

    public Integer getBrojZaposlenih()
    {
        return brojZaposlenih;
    }

    public void setBrojZaposlenih(Integer brojZaposlenih)
    {
        this.brojZaposlenih = brojZaposlenih;
    }

    public String getPokazateljUspijeha()
    {
        return pokazateljUspijeha;
    }

    public void setPokazateljUspijeha(String pokazateljUspijeha)
    {
        this.pokazateljUspijeha = pokazateljUspijeha;
    }
}
