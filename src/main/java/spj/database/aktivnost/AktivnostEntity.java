package spj.database.aktivnost;

import spj.database.mjesto.MjestoEntity;
import spj.database.SpjDomainModelEntity;
import spj.database.funkcija.FunkcijaEntity;
import spj.database.izvorfinanciranja.IzvorFinanciranjaEntity;
import spj.database.program.ProgramEntity;

import javax.persistence.*;
import java.util.List;

@Entity
public class AktivnostEntity implements SpjDomainModelEntity
{
    @Id
    private Long id;
    private String naziv;
    private String zakonskaOsnova;
    private String opis;
    private String opciCilj;
    @ManyToOne(optional = false)
    private ProgramEntity programEntity;
    @ManyToOne(optional = false)
    private FunkcijaEntity funkcijaEntity;
    @ManyToOne(optional = false)
    private MjestoEntity mjestoEntity;
    @ManyToOne(optional = false)
    private IzvorFinanciranjaEntity izvorFinanciranjaEntity;
    private String pokazateljUspijeha;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNaziv()
    {
        return naziv;
    }

    public void setNaziv(String naziv)
    {
        this.naziv = naziv;
    }

    public String getZakonskaOsnova()
    {
        return zakonskaOsnova;
    }

    public void setZakonskaOsnova(String zakonskaOsnova)
    {
        this.zakonskaOsnova = zakonskaOsnova;
    }

    public String getOpis()
    {
        return opis;
    }

    public void setOpis(String opis)
    {
        this.opis = opis;
    }

    public String getOpciCilj()
    {
        return opciCilj;
    }

    public void setOpciCilj(String opciCilj)
    {
        this.opciCilj = opciCilj;
    }

    public ProgramEntity getProgramEntity()
    {
        return programEntity;
    }

    public void setProgramEntity(ProgramEntity programEntity)
    {
        this.programEntity = programEntity;
    }

    public String getPokazateljUspijeha()
    {
        return pokazateljUspijeha;
    }

    public void setPokazateljUspijeha(String pokazateljUspijeha)
    {
        this.pokazateljUspijeha = pokazateljUspijeha;
    }

    public FunkcijaEntity getFunkcijaEntity()
    {
        return funkcijaEntity;
    }

    public void setFunkcijaEntity(FunkcijaEntity funkcijaEntity)
    {
        this.funkcijaEntity = funkcijaEntity;
    }

    public MjestoEntity getMjestoEntity()
    {
        return mjestoEntity;
    }

    public void setMjestoEntity(MjestoEntity mjestoEntity)
    {
        this.mjestoEntity = mjestoEntity;
    }

    public IzvorFinanciranjaEntity getIzvorFinanciranjaEntity()
    {
        return izvorFinanciranjaEntity;
    }

    public void setIzvorFinanciranjaEntity(IzvorFinanciranjaEntity izvorFinanciranjaEntity)
    {
        this.izvorFinanciranjaEntity = izvorFinanciranjaEntity;
    }
}
