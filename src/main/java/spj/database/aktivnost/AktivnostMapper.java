package spj.database.aktivnost;

import spj.database.mjesto.MjestoEntity;
import spj.database.SpjDomainMap;
import spj.database.SpjDomainModelEntity;
import spj.database.SpjDomainModelMapper;
import spj.database.funkcija.FunkcijaEntity;
import spj.database.izvorfinanciranja.IzvorFinanciranjaEntity;
import spj.database.program.ProgramEntity;
import spj.shared.domain.*;
import spj.shared.util.SpjAssert;
import spj.util.SpjCastTool;

import java.util.List;

public class AktivnostMapper extends SpjDomainModelMapper
{
    @Override
    public <T extends SpjDomainModel> T createDomainModel(SpjDomainModelEntity spjDomainModelEntity)
    {
        {
            SpjAssert.isNotNull(spjDomainModelEntity, "aktivnostEntity");
            SpjAssert.isOfType(spjDomainModelEntity, AktivnostEntity.class, "aktivnostEntity");


            AktivnostEntity aktivnostEntity = SpjCastTool.cast(spjDomainModelEntity);

            Program program = SpjDomainMap.PROGRAM.getMapper().createDomainModel(aktivnostEntity.getProgramEntity());
            Funkcija funkcija = SpjDomainMap.FUNKCIJA.getMapper().createDomainModel(aktivnostEntity.getFunkcijaEntity());
            Mjesto mjesto = SpjDomainMap.MJESTO.getMapper().createDomainModel(aktivnostEntity.getMjestoEntity());
            IzvorFinanciranja izvorFinanciranja = SpjDomainMap.IZVOR_FINANCIRANJA.getMapper().createDomainModel(aktivnostEntity.getIzvorFinanciranjaEntity());


            return SpjCastTool.cast(Aktivnost.builder()
                    .id(aktivnostEntity.getId())
                    .naziv(aktivnostEntity.getNaziv())
                    .zakonskaOsnova(aktivnostEntity.getZakonskaOsnova())
                    .opis(aktivnostEntity.getOpis())
                    .opciCilj(aktivnostEntity.getOpciCilj())
                    .program(program)
                    .funkcija(funkcija)
                    .mjesto(mjesto)
                    .izvoriFinanciranja(izvorFinanciranja)
                    .pokazateljUspijeha(aktivnostEntity.getPokazateljUspijeha())
                    .build());
        }
    }

    @Override
    public <T extends SpjDomainModelEntity> T createEntity(SpjDomainModel spjDomainModel)
    {
        {
            SpjAssert.isNotNull(spjDomainModel, "aktivnost");
            SpjAssert.isOfType(spjDomainModel, Aktivnost.class, "aktivnost");

            Aktivnost aktivnost = SpjCastTool.cast(spjDomainModel);

            ProgramEntity programEntity = SpjDomainMap.PROGRAM.getMapper().createEntity(aktivnost.getProgram());
            FunkcijaEntity funkcijaEntity = SpjDomainMap.FUNKCIJA.getMapper().createEntity(aktivnost.getFunkcija());
            MjestoEntity mjestoEntity = SpjDomainMap.MJESTO.getMapper().createEntity(aktivnost.getMjesto());
            IzvorFinanciranjaEntity izvorFinanciranjaEntity = SpjDomainMap.IZVOR_FINANCIRANJA.getMapper().createEntity(aktivnost.getIzvorFinanciranja());


            AktivnostEntity aktivnostEntity = new AktivnostEntity();
            aktivnostEntity.setId(aktivnost.getId());
            aktivnostEntity.setNaziv(aktivnost.getNaziv());
            aktivnostEntity.setZakonskaOsnova(aktivnost.getZakonskaOsnova());
            aktivnostEntity.setOpis(aktivnost.getOpis());
            aktivnostEntity.setOpciCilj(aktivnost.getOpciCilj());
            aktivnostEntity.setProgramEntity(programEntity);
            aktivnostEntity.setFunkcijaEntity(funkcijaEntity);
            aktivnostEntity.setMjestoEntity(mjestoEntity);
            aktivnostEntity.setIzvorFinanciranjaEntity(izvorFinanciranjaEntity);
            aktivnostEntity.setPokazateljUspijeha(aktivnost.getPokazateljUspijeha());

            return SpjCastTool.cast(aktivnostEntity);
        }
    }
}
