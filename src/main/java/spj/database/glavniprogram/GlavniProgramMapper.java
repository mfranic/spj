package spj.database.glavniprogram;

import spj.database.SpjDomainModelEntity;
import spj.database.SpjDomainModelMapper;
import spj.shared.domain.GlavniProgram;
import spj.shared.domain.SpjDomainModel;
import spj.shared.util.SpjAssert;
import spj.util.SpjCastTool;

public class GlavniProgramMapper extends SpjDomainModelMapper
{
    @Override
    public <T extends SpjDomainModel> T createDomainModel(SpjDomainModelEntity spjDomainModelEntity)
    {
        SpjAssert.isNotNull(spjDomainModelEntity, "spjDomainModelEntity");

        GlavniProgramEntity glavniProgramEntity = SpjCastTool.cast(spjDomainModelEntity);

        return SpjCastTool.cast(GlavniProgram.builder()
                .id(glavniProgramEntity.getId())
                .naziv(glavniProgramEntity.getNaziv())
                .build());
    }

    @Override
    public <T extends SpjDomainModelEntity> T createEntity(SpjDomainModel spjDomainModel)
    {
        SpjAssert.isNotNull(spjDomainModel, "spjDomainModel");

        GlavniProgram glavniProgram = SpjCastTool.cast(spjDomainModel);

        GlavniProgramEntity glavniProgramEntity = new GlavniProgramEntity();
        glavniProgramEntity.setId(glavniProgram.getId());
        glavniProgramEntity.setNaziv(glavniProgram.getNaziv());

        return SpjCastTool.cast(glavniProgramEntity);
    }
}
