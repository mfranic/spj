package spj.database.razdjel;

import spj.database.SpjDomainModelEntity;
import spj.database.SpjDomainModelMapper;
import spj.shared.domain.Razdjel;
import spj.shared.domain.SpjDomainModel;
import spj.shared.util.SpjAssert;
import spj.util.SpjCastTool;

public class RazdjelMapper extends SpjDomainModelMapper
{
    @Override
    public <T extends SpjDomainModel> T createDomainModel(SpjDomainModelEntity spjDomainModelEntity)
    {
        SpjAssert.isNotNull(spjDomainModelEntity, "razdjelEntity");
        SpjAssert.isOfType(spjDomainModelEntity, RazdjelEntity.class, "razdjelEntity");

        RazdjelEntity razdjelEntity = SpjCastTool.cast(spjDomainModelEntity);

        return SpjCastTool.cast(Razdjel.builder()
                .id(razdjelEntity.getId().toString())
                .naziv(razdjelEntity.getNaziv())
                .build());
    }

    @Override
    public <T extends SpjDomainModelEntity> T createEntity(SpjDomainModel spjDomainModel)
    {
        SpjAssert.isNotNull(spjDomainModel, "razdjel");
        SpjAssert.isOfType(spjDomainModel, Razdjel.class, "razdjel");

        Razdjel razdjel = SpjCastTool.cast(spjDomainModel);

        RazdjelEntity razdjelEntity = new RazdjelEntity();
        razdjelEntity.setId(razdjel.getId());
        razdjelEntity.setNaziv(razdjel.getNaziv());

        return SpjCastTool.cast(razdjelEntity);
    }
}
