package spj.database.izvorfinanciranja;

import spj.database.SpjDomainModelEntity;
import spj.database.SpjDomainModelMapper;
import spj.shared.domain.IzvorFinanciranja;
import spj.shared.domain.SpjDomainModel;
import spj.shared.util.SpjAssert;
import spj.util.SpjCastTool;

public class IzvorFinanciranjaMapper extends SpjDomainModelMapper
{
    @Override
    public <T extends SpjDomainModel> T createDomainModel(SpjDomainModelEntity spjDomainModelEntity)
    {
        SpjAssert.isNotNull(spjDomainModelEntity, "spjDomainModelEntity");

        IzvorFinanciranjaEntity izvorFinanciranjaEntity = SpjCastTool.cast(spjDomainModelEntity);

        return SpjCastTool.cast(IzvorFinanciranja.builder()
                .id(izvorFinanciranjaEntity.getId().toString())
                .naziv(izvorFinanciranjaEntity.getNaziv())
                .build());
    }

    @Override
    public <T extends SpjDomainModelEntity> T createEntity(SpjDomainModel spjDomainModel)
    {
        SpjAssert.isNotNull(spjDomainModel, "spjDomainModel");

        IzvorFinanciranja izvorFinanciranja = SpjCastTool.cast(spjDomainModel);

        IzvorFinanciranjaEntity izvorFinanciranjaEntity = new IzvorFinanciranjaEntity();
        izvorFinanciranjaEntity.setId(izvorFinanciranja.getId());
        izvorFinanciranjaEntity.setNaziv(izvorFinanciranja.getNaziv());

        return SpjCastTool.cast(izvorFinanciranjaEntity);
    }
}
