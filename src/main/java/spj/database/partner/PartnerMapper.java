package spj.database.partner;

import spj.database.mjesto.MjestoEntity;
import spj.database.SpjDomainMap;
import spj.database.SpjDomainModelEntity;
import spj.database.SpjDomainModelMapper;
import spj.shared.domain.Mjesto;
import spj.shared.domain.Partner;
import spj.shared.domain.SpjDomainModel;
import spj.shared.util.SpjAssert;
import spj.util.SpjCastTool;

public class PartnerMapper extends SpjDomainModelMapper
{
    @Override
    public <T extends SpjDomainModel> T createDomainModel(SpjDomainModelEntity spjDomainModelEntity)
    {
        SpjAssert.isNotNull(spjDomainModelEntity, "partnerEntity");
        SpjAssert.isOfType(spjDomainModelEntity, PartnerEntity.class, "partnerEntity");

        PartnerEntity partnerEntity = SpjCastTool.cast(spjDomainModelEntity);

        Mjesto mjesto = SpjDomainMap.MJESTO.getMapper().createDomainModel(partnerEntity.getMjestoEntity());

        return SpjCastTool.cast(Partner.builder()
                .id(partnerEntity.getId().toString())
                .naziv(partnerEntity.getNaziv())
                .telefon(partnerEntity.getTelefon())
                .odgovornaOsoba(partnerEntity.getOdgovornaOsoba())
                .fax(partnerEntity.getFax())
                .ziroRacun(partnerEntity.getZiroRacun())
                .oib(partnerEntity.getOib())
                .email(partnerEntity.getEmail())
                .adresa(partnerEntity.getAdresa())
                .mjesto(mjesto)
                .build());
    }

    @Override
    public <T extends SpjDomainModelEntity> T createEntity(SpjDomainModel spjDomainModel)
    {
        SpjAssert.isNotNull(spjDomainModel, "partner");
        SpjAssert.isOfType(spjDomainModel, Partner.class, "partner");

        Partner partner = SpjCastTool.cast(spjDomainModel);

        MjestoEntity mjestoEntity = SpjDomainMap.MJESTO.getMapper().createEntity(partner.getMjesto());

        PartnerEntity partnerEntity = new PartnerEntity();
        partnerEntity.setId(partner.getId());
        partnerEntity.setNaziv(partner.getNaziv());
        partnerEntity.setTelefon(partner.getTelefon());
        partnerEntity.setOdgovornaOsoba(partner.getOdgovornaOsoba());
        partnerEntity.setFax(partner.getFax());
        partnerEntity.setZiroRacun(partner.getZiroRacun());
        partnerEntity.setOib(partner.getOib());
        partnerEntity.setEmail(partner.getEmail());
        partnerEntity.setAdresa(partner.getAdresa());
        partnerEntity.setMjestoEntity(mjestoEntity);

        return SpjCastTool.cast(partnerEntity);
    }
}
