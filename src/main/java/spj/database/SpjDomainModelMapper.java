package spj.database;

import com.google.common.collect.ImmutableList;
import spj.shared.domain.SpjDomainModel;
import spj.shared.util.SpjAssert;

import java.util.List;

public abstract class SpjDomainModelMapper
{
    public <T extends SpjDomainModel> List<T> createDomainModel(Iterable<? extends SpjDomainModelEntity> spjDomainModelEntityList)
    {
        SpjAssert.isNotNull(spjDomainModelEntityList, "spjDomainModelEntityList");

        ImmutableList.Builder<T> spjDomainModelListBuilder = ImmutableList.builder();

        for (SpjDomainModelEntity spjDomainModelEntity : spjDomainModelEntityList)
        {
            T spjDomainModel =   createDomainModel(spjDomainModelEntity);
            spjDomainModelListBuilder.add(spjDomainModel);
        }

        return spjDomainModelListBuilder.build();
    }

    public <T extends SpjDomainModelEntity> List<T> createEntity(Iterable<? extends SpjDomainModel> spjDomainModelList)
    {
        SpjAssert.isNotNull(spjDomainModelList, "spjDomainModelList");

        ImmutableList.Builder<T> spjDomainModelEntityListBuilder = ImmutableList.builder();

        for (SpjDomainModel spjDomainModel : spjDomainModelList)
        {
            T spjDomainModelEntity =   createEntity(spjDomainModel);
            spjDomainModelEntityListBuilder.add(spjDomainModelEntity);
        }

        return spjDomainModelEntityListBuilder.build();
    }

    abstract public <T extends SpjDomainModel> T createDomainModel(SpjDomainModelEntity spjDomainModelEntity);

    abstract public <T extends SpjDomainModelEntity> T createEntity(SpjDomainModel spjDomainModel);
}
