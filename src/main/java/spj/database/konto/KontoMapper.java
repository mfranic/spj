package spj.database.konto;

import spj.database.SpjDomainModelEntity;
import spj.database.SpjDomainModelMapper;
import spj.shared.domain.Konto;
import spj.shared.domain.SpjDomainModel;
import spj.shared.util.SpjAssert;
import spj.util.SpjCastTool;

public class KontoMapper extends SpjDomainModelMapper
{
    @Override
    public <T extends SpjDomainModel> T createDomainModel(SpjDomainModelEntity spjDomainModelEntity)
    {
        SpjAssert.isNotNull(spjDomainModelEntity, "spjDomainModelEntity");

        KontoEntity kontoEntity = SpjCastTool.cast(spjDomainModelEntity);

        return SpjCastTool.cast(Konto.builder()
                .id(kontoEntity.getId().toString())
                .naziv(kontoEntity.getNaziv())
                .imaSaldokonto(kontoEntity.getImaSaldokonto())
                .koristiSe(kontoEntity.getKoristiSe())
                .obveza(kontoEntity.getObveza())
                .potrazivanje(kontoEntity.getPotrazivanje())
                .build());
    }

    @Override
    public <T extends SpjDomainModelEntity> T createEntity(SpjDomainModel spjDomainModel)
    {
        SpjAssert.isNotNull(spjDomainModel, "spjDomainModel");

        Konto konto = SpjCastTool.cast(spjDomainModel);

        KontoEntity kontoEntity = new KontoEntity();
        kontoEntity.setId(konto.getId());
        kontoEntity.setNaziv(konto.getNaziv());
        kontoEntity.setImaSaldokonto(konto.getImaSaldokonto());
        kontoEntity.setKoristiSe(konto.getKoristiSe());
        kontoEntity.setObveza(konto.getObveza());
        kontoEntity.setPotrazivanje(konto.getPotrazivanje());

        return SpjCastTool.cast(kontoEntity);
    }
}
