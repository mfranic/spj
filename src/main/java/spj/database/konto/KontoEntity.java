package spj.database.konto;

import spj.database.SpjDomainModelEntity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class KontoEntity implements SpjDomainModelEntity
{
    @Id
    private Long id;
    private String naziv;
    private Boolean imaSaldokonto;
    private Boolean koristiSe;
    private Boolean obveza;
    private Boolean potrazivanje;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNaziv()
    {
        return naziv;
    }

    public void setNaziv(String naziv)
    {
        this.naziv = naziv;
    }

    public Boolean getImaSaldokonto()
    {
        return imaSaldokonto;
    }

    public void setImaSaldokonto(Boolean imaSaldokonto)
    {
        this.imaSaldokonto = imaSaldokonto;
    }

    public Boolean getKoristiSe()
    {
        return koristiSe;
    }

    public void setKoristiSe(Boolean koristiSe)
    {
        this.koristiSe = koristiSe;
    }

    public Boolean getObveza()
    {
        return obveza;
    }

    public void setObveza(Boolean obveza)
    {
        this.obveza = obveza;
    }

    public Boolean getPotrazivanje()
    {
        return potrazivanje;
    }

    public void setPotrazivanje(Boolean potrazivanje)
    {
        this.potrazivanje = potrazivanje;
    }
}
