package spj.database;

import spj.shared.util.SpjAssert;

public class SpjQueryStringBuilder
{
    private static final String SELECT = "SELECT x FROM [from] x";
    private static final String WHERE = " WHERE x.[field]";
    private static final String IS = " [operator] [value]";
    private static final String EQUALS = "=";

    private String query;

    private SpjQueryStringBuilder()
    {
    }

    public static SpjQueryStringBuilder builder()
    {
        return new SpjQueryStringBuilder();
    }

    public SpjQueryStringBuilder select(Class<? extends SpjDomainModelEntity> domainModelClass)
    {
        SpjAssert.isNotNull(domainModelClass, "domainModelClass");

        this.query = SELECT.replace("[from]", domainModelClass.getSimpleName());
        return this;
    }

    public SpjQueryStringBuilder where(String field)
    {
        SpjAssert.isNotNullNorEmpty(field, "field");

        this.query += WHERE.replace("[field]", field);
        return this;
    }

    public SpjQueryStringBuilder is(String value)
    {
        SpjAssert.isNotNullNorEmpty(value, "value");

        this.query += IS.replace("[operator]", EQUALS).replace("[value]", value);
        return this;
    }

    public String build()
    {
        return this.query;
    }
}
