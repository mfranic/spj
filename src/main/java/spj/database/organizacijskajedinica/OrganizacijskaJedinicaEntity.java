package spj.database.organizacijskajedinica;

import spj.database.SpjDomainModelEntity;
import spj.database.korisnikproracuna.KorisnikProracunaEntity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class OrganizacijskaJedinicaEntity implements SpjDomainModelEntity
{
    @Id
    private Long id;
    private String naziv;

    @ManyToOne( optional = false)
    private KorisnikProracunaEntity korisnikProracunaEntity;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNaziv()
    {
        return naziv;
    }

    public void setNaziv(String naziv)
    {
        this.naziv = naziv;
    }

    public KorisnikProracunaEntity getKorisnikProracunaEntity()
    {
        return korisnikProracunaEntity;
    }

    public void setKorisnikProracunaEntity(KorisnikProracunaEntity korisnikProracunaEntity)
    {
        this.korisnikProracunaEntity = korisnikProracunaEntity;
    }
}
