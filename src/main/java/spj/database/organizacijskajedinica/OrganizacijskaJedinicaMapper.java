package spj.database.organizacijskajedinica;

import spj.database.SpjDomainMap;
import spj.database.SpjDomainModelEntity;
import spj.database.SpjDomainModelMapper;
import spj.database.korisnikproracuna.KorisnikProracunaEntity;
import spj.shared.domain.KorisnikProracuna;
import spj.shared.domain.OrganizacijskaJedinica;
import spj.shared.domain.SpjDomainModel;
import spj.shared.util.SpjAssert;
import spj.util.SpjCastTool;

public class OrganizacijskaJedinicaMapper extends SpjDomainModelMapper
{
    @Override
    public <T extends SpjDomainModel> T createDomainModel(SpjDomainModelEntity spjDomainModelEntity)
    {
        SpjAssert.isNotNull(spjDomainModelEntity, "spjDomainModelEntity");

        OrganizacijskaJedinicaEntity organizacijskaJedinicaEntity = SpjCastTool.cast(spjDomainModelEntity);

        KorisnikProracuna korisnikProracuna = SpjDomainMap.getMap(KorisnikProracuna.class).getMapper().createDomainModel(organizacijskaJedinicaEntity.getKorisnikProracunaEntity());

        return SpjCastTool.cast(OrganizacijskaJedinica.builder()
                .id(organizacijskaJedinicaEntity.getId().toString())
                .naziv(organizacijskaJedinicaEntity.getNaziv())
                .korisnikProracuna(korisnikProracuna)
                .build());
    }

    @Override
    public <T extends SpjDomainModelEntity> T createEntity(SpjDomainModel spjDomainModel)
    {
        SpjAssert.isNotNull(spjDomainModel, "spjDomainModel");

        OrganizacijskaJedinica organizacijskaJedinica = SpjCastTool.cast(spjDomainModel);

        KorisnikProracunaEntity korisnikProracunaEntity = SpjDomainMap.getMap(KorisnikProracuna.class).getMapper().createEntity(organizacijskaJedinica.getKorisnikProracuna());

        OrganizacijskaJedinicaEntity organizacijskaJedinicaEntity = new OrganizacijskaJedinicaEntity();
        organizacijskaJedinicaEntity.setId(organizacijskaJedinica.getId());
        organizacijskaJedinicaEntity.setNaziv(organizacijskaJedinica.getNaziv());
        organizacijskaJedinicaEntity.setKorisnikProracunaEntity(korisnikProracunaEntity);

        return SpjCastTool.cast(organizacijskaJedinicaEntity);
    }
}
