package spj.database;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import spj.shared.domain.SpjDomainModel;
import spj.shared.util.SpjAssert;
import spj.shared.util.SpjEvaluate;
import spj.util.SpjCastTool;
import spj.util.SpjStringTool;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class SpjRepository
{
    @PersistenceContext
    EntityManager entityManager;

    @Transactional
    public <T extends SpjDomainModel> void create(T spjDomainModel) throws NoSuchFieldException
    {
        SpjAssert.isNotNull(spjDomainModel, "spjDomainModel");

        SpjDomainModelEntity spjDomainModelEntity = SpjDomainMap.getMap(spjDomainModel.getClass()).getMapper().createEntity(spjDomainModel);

        entityManager.persist(spjDomainModelEntity);
    }

    @Transactional
    public <T extends SpjDomainModel> T read(Class<T> type, Long id) throws NoSuchFieldException
    {
        SpjAssert.isNotNull(id, "id");

        SpjDomainModelEntity result = entityManager.find(SpjDomainMap.getMap(type).getEntityClass(), id);

        if (result == null)
        {
            throw new NoSuchFieldException(type.getName() + " with id " + id + "doesn't exists");
        }

        return SpjCastTool.cast(SpjDomainMap.getMap(type).getMapper().createDomainModel(result));
    }

    @Transactional
    public boolean exists(Class<? extends SpjDomainModel> type, Long id) throws NoSuchFieldException
    {
        SpjAssert.isNotNull(id, "id");
        SpjAssert.isNotNull(type, "type");

        return SpjEvaluate.isNotNull(entityManager.find(SpjDomainMap.getMap(type).getEntityClass(), id));
    }

    @Transactional
    public <T extends SpjDomainModel> void update(T spjDomainModel) throws NoSuchFieldException
    {
        SpjAssert.isNotNull(spjDomainModel, "spjDomainModel");

        SpjDomainModelEntity spjDomainModelEntity = SpjDomainMap.getMap(spjDomainModel.getClass()).getMapper().createEntity(spjDomainModel);

        entityManager.merge(spjDomainModelEntity);
    }

    @Transactional
    public void delete(Class<? extends SpjDomainModel> type, Long id) throws NoSuchFieldException
    {
        SpjAssert.isNotNull(id, "id");
        SpjAssert.isNotNull(type, "type");

        SpjDomainModelEntity result = entityManager.find(SpjDomainMap.getMap(type).getEntityClass(), id);

        if (result == null)
        {
            throw new NoSuchFieldException(type.getName() + " with id " + id + "doesn't exists");
        }

        entityManager.remove(result);
    }

    @Transactional
    public <T extends SpjDomainModel> List<T> findAll(Class<T> type) throws NoSuchFieldException
    {
        SpjAssert.isNotNull(type, "type");

        TypedQuery<? extends SpjDomainModelEntity> query = entityManager.createQuery(SpjQueryStringBuilder.builder().select(SpjDomainMap.getMap(type).getEntityClass()).build(), SpjDomainMap.getMap(type).getEntityClass());

        List<? extends SpjDomainModelEntity> list = query.getResultList();

        return SpjCastTool.cast(SpjDomainMap.getMap(type).getMapper().createDomainModel(list));
    }

    @Transactional
    public Integer count(Class<? extends SpjDomainModel> type) throws NoSuchFieldException
    {
        SpjAssert.isNotNull(type, "type");

        return findAll(type).size();
    }

    @Transactional
    public <T extends SpjDomainModel> List<T> findAllChildren(SpjDomainModel parent, Class<T> childClass)
    {
        SpjAssert.isNotNull(parent, "parent");
        SpjAssert.isNotNull(childClass, "childClass");

        TypedQuery<? extends SpjDomainModelEntity> query = entityManager.createQuery(
                SpjQueryStringBuilder.builder()
                        .select(SpjDomainMap.getMap(childClass).getEntityClass())
                        .where(SpjStringTool.toCamelCase(SpjDomainMap.getMap(parent.getClass()).getEntityClass().getSimpleName()) + ".id")
                        .is(parent.getId().toString())
                        .build(),
                SpjDomainMap.getMap(childClass).getEntityClass());

        List<? extends SpjDomainModelEntity> list = query.getResultList();

        return SpjCastTool.cast(SpjDomainMap.getMap(childClass).getMapper().createDomainModel(list));
    }
}
