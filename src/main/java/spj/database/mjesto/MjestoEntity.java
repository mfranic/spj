package spj.database.mjesto;

import spj.database.SpjDomainModelEntity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MjestoEntity implements SpjDomainModelEntity
{
    @Id
    private Long id;
    private String naziv;
    private String pozivniBroj;
    private String postanskiBroj;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNaziv()
    {
        return naziv;
    }

    public void setNaziv(String naziv)
    {
        this.naziv = naziv;
    }

    public String getPozivniBroj()
    {
        return pozivniBroj;
    }

    public void setPozivniBroj(String pozivniBroj)
    {
        this.pozivniBroj = pozivniBroj;
    }

    public String getPostanskiBroj()
    {
        return postanskiBroj;
    }

    public void setPostanskiBroj(String postanskiBroj)
    {
        this.postanskiBroj = postanskiBroj;
    }
}
