package spj.database.mjesto;

import spj.database.SpjDomainModelEntity;
import spj.database.SpjDomainModelMapper;
import spj.shared.domain.Mjesto;
import spj.shared.domain.SpjDomainModel;
import spj.shared.util.SpjAssert;
import spj.util.SpjCastTool;

public class MjestoMapper extends SpjDomainModelMapper
{
    @Override
    public <T extends SpjDomainModel> T createDomainModel(SpjDomainModelEntity spjDomainModelEntity)
    {
        SpjAssert.isNotNull(spjDomainModelEntity, "mjestoEntity");
        SpjAssert.isOfType(spjDomainModelEntity, MjestoEntity.class, "mjestoEntity");

        MjestoEntity mjestoEntity = SpjCastTool.cast(spjDomainModelEntity);

        return SpjCastTool.cast(Mjesto.builder()
                .id(mjestoEntity.getId().toString())
                .naziv(mjestoEntity.getNaziv())
                .pozivniBroj(mjestoEntity.getPozivniBroj())
                .postanskiBroj(mjestoEntity.getPostanskiBroj())
                .build());
    }

    @Override
    public <T extends SpjDomainModelEntity> T createEntity(SpjDomainModel spjDomainModel)
    {
        SpjAssert.isNotNull(spjDomainModel, "mjesto");
        SpjAssert.isOfType(spjDomainModel, Mjesto.class, "mjesto");

        Mjesto mjesto = SpjCastTool.cast(spjDomainModel);

        MjestoEntity razdjelEntity = new MjestoEntity();
        razdjelEntity.setId(mjesto.getId());
        razdjelEntity.setNaziv(mjesto.getNaziv());
        razdjelEntity.setPozivniBroj(mjesto.getPozivniBroj());
        razdjelEntity.setPostanskiBroj(mjesto.getPostanskiBroj());

        return SpjCastTool.cast(razdjelEntity);    }
}
