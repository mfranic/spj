package spj.database.glava;

import spj.database.SpjDomainMap;
import spj.database.SpjDomainModelEntity;
import spj.database.SpjDomainModelMapper;
import spj.database.razdjel.RazdjelEntity;
import spj.shared.domain.Glava;
import spj.shared.domain.Razdjel;
import spj.shared.domain.SpjDomainModel;
import spj.shared.util.SpjAssert;
import spj.util.SpjCastTool;

public class GlavaMapper extends SpjDomainModelMapper
{
    @Override
    public <T extends SpjDomainModel> T createDomainModel(SpjDomainModelEntity spjDomainModelEntity)
    {
        SpjAssert.isNotNull(spjDomainModelEntity, "glavaEntity");
        SpjAssert.isOfType(spjDomainModelEntity, GlavaEntity.class, "glavaEntity");

        GlavaEntity glavaEntity = SpjCastTool.cast(spjDomainModelEntity);

        Razdjel razdjel = SpjDomainMap.RAZDJEL.getMapper().createDomainModel(glavaEntity.getRazdjelEntity());

        return SpjCastTool.cast(Glava.builder()
                .id(glavaEntity.getId())
                .naziv(glavaEntity.getNaziv())
                .razdjel(razdjel)
                .build());
    }

    @Override
    public <T extends SpjDomainModelEntity> T createEntity(SpjDomainModel spjDomainModel)
    {
        SpjAssert.isNotNull(spjDomainModel, "glava");
        SpjAssert.isOfType(spjDomainModel, Glava.class, "glava");

        Glava glava = SpjCastTool.cast(spjDomainModel);

        RazdjelEntity razdjelEntity = SpjDomainMap.RAZDJEL.getMapper().createEntity(glava.getRazdjel());

        GlavaEntity glavaEntity = new GlavaEntity();
        glavaEntity.setNaziv(glava.getNaziv());
        glavaEntity.setRazdjelEntity(razdjelEntity);
        glavaEntity.setId(glava.getId());

        return SpjCastTool.cast(glavaEntity);
    }
}
