package spj.database.glava;

import spj.database.SpjDomainModelEntity;
import spj.database.razdjel.RazdjelEntity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class GlavaEntity implements SpjDomainModelEntity
{
    @Id
    private Long id;
    private String naziv;

    @ManyToOne( optional = false)
    private RazdjelEntity razdjelEntity;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNaziv()
    {
        return naziv;
    }

    public void setNaziv(String naziv)
    {
        this.naziv = naziv;
    }

    public RazdjelEntity getRazdjelEntity()
    {
        return razdjelEntity;
    }

    public void setRazdjelEntity(RazdjelEntity razdjelEntity)
    {
        this.razdjelEntity = razdjelEntity;
    }
}
