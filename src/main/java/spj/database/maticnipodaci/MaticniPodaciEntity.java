package spj.database.maticnipodaci;

import spj.database.SpjDomainModelEntity;
import spj.database.mjesto.MjestoEntity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class MaticniPodaciEntity implements SpjDomainModelEntity
{
    @Id
    private Long id;
    private String naziv;
    private String telefon;
    private String odgovornaOsoba;
    private String fax;
    private String ziroRacun;
    private String oib;
    private String adresa;
    @ManyToOne(optional = false)
    private MjestoEntity mjestoEntity;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNaziv()
    {
        return naziv;
    }

    public void setNaziv(String naziv)
    {
        this.naziv = naziv;
    }

    public String getTelefon()
    {
        return telefon;
    }

    public void setTelefon(String telefon)
    {
        this.telefon = telefon;
    }

    public String getOdgovornaOsoba()
    {
        return odgovornaOsoba;
    }

    public void setOdgovornaOsoba(String odgovornaOsoba)
    {
        this.odgovornaOsoba = odgovornaOsoba;
    }

    public String getFax()
    {
        return fax;
    }

    public void setFax(String fax)
    {
        this.fax = fax;
    }

    public String getZiroRacun()
    {
        return ziroRacun;
    }

    public void setZiroRacun(String ziroRacun)
    {
        this.ziroRacun = ziroRacun;
    }

    public String getOib()
    {
        return oib;
    }

    public void setOib(String oib)
    {
        this.oib = oib;
    }

    public String getAdresa()
    {
        return adresa;
    }

    public void setAdresa(String adresa)
    {
        this.adresa = adresa;
    }

    public MjestoEntity getMjestoEntity()
    {
        return mjestoEntity;
    }

    public void setMjestoEntity(MjestoEntity mjestoEntity)
    {
        this.mjestoEntity = mjestoEntity;
    }
}
