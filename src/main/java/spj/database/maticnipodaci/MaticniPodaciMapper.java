package spj.database.maticnipodaci;

import spj.database.SpjDomainMap;
import spj.database.SpjDomainModelEntity;
import spj.database.SpjDomainModelMapper;
import spj.database.mjesto.MjestoEntity;
import spj.shared.domain.MaticniPodaci;
import spj.shared.domain.Mjesto;
import spj.shared.domain.SpjDomainModel;
import spj.shared.util.SpjAssert;
import spj.util.SpjCastTool;

public class MaticniPodaciMapper extends SpjDomainModelMapper
{
    @Override
    public <T extends SpjDomainModel> T createDomainModel(SpjDomainModelEntity spjDomainModelEntity)
    {
        SpjAssert.isNotNull(spjDomainModelEntity, "maticniPodaciEntity");
        SpjAssert.isOfType(spjDomainModelEntity, MaticniPodaciEntity.class, "maticniPodaciEntity");

        MaticniPodaciEntity maticniPodaciEntity = SpjCastTool.cast(spjDomainModelEntity);

        Mjesto mjesto = SpjDomainMap.MJESTO.getMapper().createDomainModel(maticniPodaciEntity.getMjestoEntity());

        return SpjCastTool.cast(MaticniPodaci.builder()
                .id(maticniPodaciEntity.getId().toString())
                .naziv(maticniPodaciEntity.getNaziv())
                .telefon(maticniPodaciEntity.getTelefon())
                .odgovornaOsoba(maticniPodaciEntity.getOdgovornaOsoba())
                .fax(maticniPodaciEntity.getFax())
                .ziroRacun(maticniPodaciEntity.getZiroRacun())
                .oib(maticniPodaciEntity.getOib())
                .adresa(maticniPodaciEntity.getAdresa())
                .mjesto(mjesto)
                .build());
    }

    @Override
    public <T extends SpjDomainModelEntity> T createEntity(SpjDomainModel spjDomainModel)
    {
        SpjAssert.isNotNull(spjDomainModel, "maticniPodaci");
        SpjAssert.isOfType(spjDomainModel, MaticniPodaci.class, "maticniPodaci");

        MaticniPodaci maticniPodaci = SpjCastTool.cast(spjDomainModel);

        MjestoEntity mjestoEntity = SpjDomainMap.MJESTO.getMapper().createEntity(maticniPodaci.getMjesto());

        MaticniPodaciEntity maticniPodaciEntity = new MaticniPodaciEntity();
        maticniPodaciEntity.setId(maticniPodaci.getId());
        maticniPodaciEntity.setNaziv(maticniPodaci.getNaziv());
        maticniPodaciEntity.setTelefon(maticniPodaci.getTelefon());
        maticniPodaciEntity.setOdgovornaOsoba(maticniPodaci.getOdgovornaOsoba());
        maticniPodaciEntity.setFax(maticniPodaci.getFax());
        maticniPodaciEntity.setZiroRacun(maticniPodaci.getZiroRacun());
        maticniPodaciEntity.setOib(maticniPodaci.getOib());
        maticniPodaciEntity.setAdresa(maticniPodaci.getAdresa());
        maticniPodaciEntity.setMjestoEntity(mjestoEntity);

        return SpjCastTool.cast(maticniPodaciEntity);
    }
}
