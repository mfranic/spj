package spj.database;

import spj.database.maticnipodaci.MaticniPodaciEntity;
import spj.database.maticnipodaci.MaticniPodaciMapper;
import spj.database.mjesto.MjestoEntity;
import spj.database.mjesto.MjestoMapper;
import spj.database.aktivnost.AktivnostEntity;
import spj.database.aktivnost.AktivnostMapper;
import spj.database.funkcija.FunkcijaEntity;
import spj.database.funkcija.FunkcijaMapper;
import spj.database.glava.GlavaEntity;
import spj.database.glava.GlavaMapper;
import spj.database.glavniprogram.GlavniProgramEntity;
import spj.database.glavniprogram.GlavniProgramMapper;
import spj.database.izvod.IzvodEntity;
import spj.database.izvod.IzvodMapper;
import spj.database.izvorfinanciranja.IzvorFinanciranjaEntity;
import spj.database.izvorfinanciranja.IzvorFinanciranjaMapper;
import spj.database.konto.KontoEntity;
import spj.database.konto.KontoMapper;
import spj.database.korisnikproracuna.KorisnikProracunaEntity;
import spj.database.korisnikproracuna.KorisnikProracunaMapper;
import spj.database.organizacijskajedinica.OrganizacijskaJedinicaEntity;
import spj.database.organizacijskajedinica.OrganizacijskaJedinicaMapper;
import spj.database.partner.PartnerEntity;
import spj.database.partner.PartnerMapper;
import spj.database.program.ProgramEntity;
import spj.database.program.ProgramMapper;
import spj.database.razdjel.RazdjelEntity;
import spj.database.razdjel.RazdjelMapper;
import spj.shared.domain.*;

public enum SpjDomainMap
{
    GLAVA(Glava.class, GlavaEntity.class, GlavaMapper.class),
    KORISNIK_PRORACUNA(KorisnikProracuna.class, KorisnikProracunaEntity.class, KorisnikProracunaMapper.class),
    IZVOR_FINANCIRANJA(IzvorFinanciranja.class, IzvorFinanciranjaEntity.class, IzvorFinanciranjaMapper.class),
    GLAVNI_PROGRAM(GlavniProgram.class, GlavniProgramEntity.class, GlavniProgramMapper.class),
    ORGANIZACIJSKA_JEDINICA(OrganizacijskaJedinica.class, OrganizacijskaJedinicaEntity.class, OrganizacijskaJedinicaMapper.class),
    KONTO(Konto.class, KontoEntity.class, KontoMapper.class),
    IZVOD(Izvod.class, IzvodEntity.class, IzvodMapper.class),
    PROGRAM(Program.class, ProgramEntity.class, ProgramMapper.class),
    RAZDJEL(Razdjel.class, RazdjelEntity.class, RazdjelMapper.class),
    FUNKCIJA(Funkcija.class, FunkcijaEntity.class, FunkcijaMapper.class),
    MJESTO(Mjesto.class, MjestoEntity.class, MjestoMapper.class),
    AKTIVNOST(Aktivnost.class, AktivnostEntity.class, AktivnostMapper.class),
    PARTNER(Partner.class, PartnerEntity.class, PartnerMapper.class),
    MATICNI_PODACI(MaticniPodaci.class, MaticniPodaciEntity.class, MaticniPodaciMapper.class);

    private final Class<? extends SpjDomainModel> domainModelClass;
    private final Class<? extends SpjDomainModelEntity> entityClass;
    private final Class<? extends SpjDomainModelMapper> mapperClass;

    private SpjDomainMap(Class<? extends SpjDomainModel> domainModelClass,
                         Class<? extends SpjDomainModelEntity> entityClass,
                         Class<? extends SpjDomainModelMapper> mapperClass)
    {
        this.domainModelClass = domainModelClass;
        this.entityClass = entityClass;
        this.mapperClass = mapperClass;
    }

    public Class<? extends SpjDomainModel> getDomainModelClass()
    {
        return this.domainModelClass;
    }

    public Class<? extends SpjDomainModelEntity> getEntityClass()
    {
        return this.entityClass;
    }

    public SpjDomainModelMapper getMapper()
    {
        try
        {
            return mapperClass.newInstance();
        }
        catch (Exception exception)
        {
            throw new RuntimeException(exception);
        }
    }

    public static SpjDomainMap getMap(Class<?> type)
    {
        for (SpjDomainMap map : SpjDomainMap.values())
        {
            if (map.getDomainModelClass().equals(type) || map.getEntityClass().equals(type))
            {
                return map;
            }
        }
        throw new RuntimeException("There's no SpjDomainMap for requested class: " + type.getName());
    }
}
