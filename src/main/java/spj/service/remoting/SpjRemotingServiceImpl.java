package spj.service.remoting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spj.database.SpjRepository;
import spj.shared.domain.SpjDomainModel;
import spj.shared.remoting.service.SpjRemotingService;
import spj.shared.util.SpjAssert;

import java.util.List;

@Service
public class SpjRemotingServiceImpl implements SpjRemotingService
{
    private SpjRepository spjRepository;

    @Autowired
    public SpjRemotingServiceImpl(SpjRepository spjRepository)
    {
        SpjAssert.isNotNull(spjRepository, "spjRepository");

        this.spjRepository = spjRepository;
    }

    @Override
    public void create(SpjDomainModel spjDomainModel)
    {
        SpjAssert.isNotNull(spjDomainModel, "spjDomainModel");

        try
        {
            spjRepository.create(spjDomainModel);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T extends SpjDomainModel> T read(Long id, Class<T> domainModelClass)
    {
        SpjAssert.isNotNull(id, "id");
        SpjAssert.isNotNull(domainModelClass, "domainModelClass");

        try
        {
            return spjRepository.read(domainModelClass, id);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean exists(Long id, Class<? extends SpjDomainModel> domainModelClass)
    {
        SpjAssert.isNotNull(id, "id");
        SpjAssert.isNotNull(domainModelClass, "domainModelClass");

        try
        {
            return spjRepository.exists(domainModelClass, id);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int count(Class<? extends SpjDomainModel> domainModelClass)
    {
        SpjAssert.isNotNull(domainModelClass, "domainModelClass");

        try
        {
            return spjRepository.count(domainModelClass);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(SpjDomainModel spjDomainModel)
    {
        SpjAssert.isNotNull(spjDomainModel, "spjDomainModel");

        try
        {
            spjRepository.update(spjDomainModel);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(Long id, Class<? extends SpjDomainModel> domainModelClass)
    {
        SpjAssert.isNotNull(id, "id");
        SpjAssert.isNotNull(domainModelClass, "domainModelClass");

        try
        {
            spjRepository.delete(domainModelClass, id);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T extends SpjDomainModel> List<T> findAll(Class<T> domainModelClass)
    {
        SpjAssert.isNotNull(domainModelClass, "spjDomainModel");

        try
        {
            return spjRepository.findAll(domainModelClass);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T extends SpjDomainModel> List<T> findAllChildren(SpjDomainModel parent, Class<T> childClass)
    {
        SpjAssert.isNotNull(parent, "parent");
        SpjAssert.isNotNull(childClass, "childClass");

        try
        {
            return spjRepository.findAllChildren(parent, childClass);
        }
        catch (Exception exception)
        {
            throw new RuntimeException(exception);
        }

    }

}
