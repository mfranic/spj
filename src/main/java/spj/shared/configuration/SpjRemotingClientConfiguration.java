package spj.shared.configuration;

import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;
import spj.shared.remoting.service.SpjRemotingService;

public class SpjRemotingClientConfiguration
{

    private final String serviceUrl;

    private SpjRemotingClientConfiguration(String url)
    {
        this.serviceUrl = url;
    }

    public SpjRemotingService getService()
    {
        return createProxy(serviceUrl, SpjRemotingService.class);
    }

    @SuppressWarnings("unchecked")
    private <T> T createProxy(String serviceUrl, Class<T> remotingClass)
    {
        HttpInvokerProxyFactoryBean proxyFactory = new HttpInvokerProxyFactoryBean();
        proxyFactory.setServiceUrl(serviceUrl + "/remoting/spjremotingservice");
        proxyFactory.setServiceInterface(remotingClass);
        proxyFactory.afterPropertiesSet();
        return (T) proxyFactory.getObject();
    }

    public static SpjRemotingClientConfiguration build(String serviceUrl)
    {
        if (serviceUrl == null || serviceUrl.isEmpty())
        {
            throw new IllegalArgumentException("serviceUrl can't be null or empty");
        }
        return new SpjRemotingClientConfiguration(serviceUrl);
    }

}
