package spj.shared.remoting.service;

import spj.shared.domain.SpjDomainModel;

import java.util.List;

public interface SpjRemotingService
{
    public void create(SpjDomainModel spjDomainModel);

    public <T extends SpjDomainModel> T read(Long id, Class<T> domainModelClass);

    public boolean exists(Long id, Class<? extends SpjDomainModel> domainModelClass);

    public int count(Class<? extends SpjDomainModel> domainModelClass);

    public void update(SpjDomainModel spjDomainModel);

    public void delete(Long id, Class<? extends SpjDomainModel> domainModelClass);

    public <T extends SpjDomainModel> List<T> findAll(Class<T> domainModelClass);

    public <T extends SpjDomainModel> List<T> findAllChildren(SpjDomainModel parent, Class<T> childClass);

}