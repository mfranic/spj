package spj.shared.util;

public class SpjAssert
{
    private static final String NOT_NULL = "%s can't be null";

    private static final String NOT_NULL_OR_EMPTY = "%s can't be null or empty";

    private static final String MUST_BE_OF_TYPE = "%s must be of type %s";

    private static final String MUST_BE_GREATER_THAN = "%s must be greater than %s";

    private static final String MUST_BE_LESSER_THAN = "%s must be lesser than %s";

    public static void isNotNull(Object value, String argumentName)
    {
        if (!SpjEvaluate.isNotNull(value))
        {
            throw new IllegalArgumentException(String.format(NOT_NULL, argumentName));
        }
    }

    public static void isNotNullNorEmpty(String value, String argumentName)
    {
        if (!SpjEvaluate.isNotNullNorEmpty(value))
        {
            throw new IllegalArgumentException(String.format(NOT_NULL_OR_EMPTY, argumentName));
        }
    }

    public static void isNotNullNorEmpty(Iterable<? extends Object> value, String argumentName)
    {
        if (!SpjEvaluate.isNotNullNorEmpty(value))
        {
            throw new IllegalArgumentException(String.format(NOT_NULL_OR_EMPTY, argumentName));
        }
    }

    public static void isOfType(Object value, Class<?> type, String argumentName)
    {
        if (!SpjEvaluate.isOfType(value, type))
        {
            throw new IllegalArgumentException(String.format(MUST_BE_OF_TYPE, argumentName, type.getName()));
        }
    }

    public static <T> void isGreaterThan(Comparable<T> value, T minValue, String argumentName)
    {
        if(!SpjEvaluate.isGreaterThan(value,minValue))
        {
            throw new IllegalArgumentException(String.format(MUST_BE_GREATER_THAN,argumentName, minValue ));
        }
    }

    public static <T> void isLesserThan(Comparable<T> value, T minValue, String argumentName)
    {
        if(!SpjEvaluate.isLesserThan(value,minValue))
        {
            throw new IllegalArgumentException(String.format(MUST_BE_LESSER_THAN,argumentName, minValue ));
        }
    }
}
