package spj.shared.util;

public class SpjEvaluate
{
    public static boolean isNotNull(Object value)
    {
        return value != null;
    }

    public static boolean isNotNullNorEmpty(String value)
    {
        return isNotNull(value) && !value.isEmpty();
    }

    public static boolean isNotNullNorEmpty(Iterable<? extends Object> value)
    {
        return isNotNull(value) && value.iterator().hasNext();
    }

    public static boolean isOfType(Object value, Class<? extends Object> type)
    {
        SpjAssert.isNotNull(value, "value");

        return value.getClass().equals(type);
    }

    public static <T> boolean isGreaterThan(Comparable<T> value, T minValue)
    {
        SpjAssert.isNotNull(value, "value");
        SpjAssert.isNotNull(minValue, "minValue");

        return isNotNull(value) && isNotNull(minValue) && value.compareTo(minValue) > 0;
    }

    public static <T> boolean isLesserThan(Comparable<T> value, T maxValue)
    {
        SpjAssert.isNotNull(value, "value");
        SpjAssert.isNotNull(maxValue, "maxValue");

        return isNotNull(value) && isNotNull(maxValue) && value.compareTo(maxValue) < 0;
    }
}
