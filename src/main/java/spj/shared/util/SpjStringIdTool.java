package spj.shared.util;

public class SpjStringIdTool
{
    public static final int BEGIN_INDEX = 0;
    public static final int SECOND_INDEX = 1;

    public static Long stringWithBeginingCharacterToLong(String value, int noOfDigitsAfterCharacter)
    {
        SpjAssert.isNotNullNorEmpty(value, "value");
        SpjAssert.isGreaterThan(noOfDigitsAfterCharacter, 0, "noOfDigitsAfterCharacter");

        int beginingCharacterId = (int) value.substring(BEGIN_INDEX, SECOND_INDEX).charAt(BEGIN_INDEX);

        return Long.valueOf(beginingCharacterId * noOfDigitsAfterCharacter) + Long.valueOf(value.substring(SECOND_INDEX));
    }

    public static String stringWithBeginingCharacterFromLong(Long value, int noOfDigitsAfterCharacter)
    {
        SpjAssert.isNotNull(value, "value");
        SpjAssert.isGreaterThan(noOfDigitsAfterCharacter, 0, "noOfDigitsAfterCharacter");

        char beginingCharacter = (char) (value / noOfDigitsAfterCharacter);
        Long digitPart = value % noOfDigitsAfterCharacter;

        return beginingCharacter + digitPart.toString();
    }
}
