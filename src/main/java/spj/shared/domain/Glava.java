package spj.shared.domain;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import spj.shared.util.SpjAssert;
import spj.shared.util.SpjEvaluate;

import java.util.List;

public class Glava extends SpjCodebookModel
{
    private final Razdjel razdjel;

    private Glava(Long id, String naziv, Razdjel razdjel)
    {
        super(id, naziv);
        this.razdjel = razdjel;
    }

    @Override
    public String getIdString()
    {
        return this.id.toString();
    }

    public Razdjel getRazdjel()
    {
        return razdjel;
    }

    @Override
    public boolean equals(Object otherAsObject)
    {

        if (this == otherAsObject)
        {
            return true;
        }
        if (otherAsObject == null)
        {
            return false;
        }

        if (getClass() != otherAsObject.getClass())
        {
            return false;
        }

        Glava other = (Glava) otherAsObject;

        return Objects.equal(this.id, other.id) &&
                Objects.equal(this.naziv, other.naziv) &&
                Objects.equal(this.razdjel, other.razdjel);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(this.id, this.naziv, this.razdjel);
    }

    public static Glava.Builder builder()
    {
        return new Glava.Builder();
    }

    public static Glava.Builder builder(Glava glava)
    {
        SpjAssert.isNotNull(glava, "glava");

        Glava.Builder builder = new Glava.Builder();
        builder.id(glava.getId());
        builder.naziv(glava.getNaziv());
        builder.razdjel(glava.getRazdjel());

        return builder;
    }

    public static class Builder extends SpjCodebookModel.Builder
    {
        private Long partialId;
        private Razdjel razdjel;

        @Override
        public Glava.Builder id(String id)
        {
            SpjAssert.isNotNull(id, "id");

            if (!id.isEmpty())
            {
                this.partialId = Long.valueOf(id);
            }

            return this;
        }

        public Glava.Builder id(Long id)
        {
            SpjAssert.isNotNull(id, "id");

            this.id = id;

            return this;
        }

        @Override
        public Glava.Builder naziv(String naziv)
        {
            SpjAssert.isNotNull(naziv, "naziv");

            this.naziv = naziv;

            return this;
        }

        public Glava.Builder razdjel(Razdjel razdjel)
        {
            SpjAssert.isNotNull(razdjel, "razdjel");

            this.razdjel = razdjel;

            return this;
        }

        public Glava build()
        {
            if (!validate().isEmpty())
            {
                throw new IllegalStateException("Trying to build invalid Glava");
            }

            Long glavaId = this.id != null ? this.id : this.razdjel.getId() * 100 + this.partialId;

            return new Glava(glavaId, this.naziv, this.razdjel);
        }

        @Override
        public List<String> validate()
        {
            ImmutableList.Builder<String> resultBuilder = ImmutableList.builder();

            if (!SpjEvaluate.isNotNullNorEmpty(this.naziv))
            {
                resultBuilder.add("Naziv glave ne smije biti prazan");
            }

            if(!SpjEvaluate.isNotNull(this.razdjel))
            {
                resultBuilder.add("Glava mora pripadati razdjelu!");
            }

            if ( SpjEvaluate.isNotNull(this.id) && (!SpjEvaluate.isGreaterThan(this.id, 0L) || !SpjEvaluate.isLesserThan(this.id, 100000L)))
            {
                resultBuilder.add("Sifra glave mora biti pozitivan peteroznamenkasti broj!");
            }

            if ( SpjEvaluate.isNotNull(this.partialId) && (!SpjEvaluate.isGreaterThan(this.partialId, 0L) || !SpjEvaluate.isLesserThan(this.partialId, 100L)))
            {
                resultBuilder.add("Dio sifre glave koji oznacava glavu mora biti pozitivan dvoznamenkasti broj!");
            }

            return resultBuilder.build();
        }

        @Override
        public Glava defaultValue()
        {
            return new Glava(0L, "", Razdjel.builder().defaultValue());
        }
    }
}
