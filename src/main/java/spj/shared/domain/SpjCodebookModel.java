package spj.shared.domain;

import java.io.Serializable;
import java.util.List;

public abstract class SpjCodebookModel implements SpjDomainModel, Serializable
{
    private static final long serialVersionUID = 1L;

    protected final Long id;

    protected final String naziv;

    protected SpjCodebookModel(Long id, String naziv)
    {
        this.id = id;
        this.naziv = naziv;
    }

    @Override
    public Long getId()
    {
        return this.id;
    }

    @Override
    public String getStringIdentifier()
    {
        return this.naziv;
    }
    public String getIdString()
    {
        return id.toString();
    }

    public String getNaziv()
    {
        return this.naziv;
    }

    public static abstract class Builder implements SpjDomainModel.Builder
    {
        protected Long id;

        protected String naziv;

        abstract public Builder id(String id);

        abstract public Builder naziv(String naziv);

        abstract public List<String> validate();

        abstract public <T extends SpjDomainModel> T build();

        abstract public <T extends SpjDomainModel> T defaultValue();
    }
}