package spj.shared.domain;

import java.util.List;

public interface SpjDomainModel
{
    public Long getId();

    public String getStringIdentifier();

    public interface Builder
    {
        public Builder id(String id);

        public List<String> validate();

        public <T extends SpjDomainModel> T build();

        public <T extends SpjDomainModel> T defaultValue();
    }
}
