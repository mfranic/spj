package spj.shared.domain;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import spj.shared.util.SpjAssert;
import spj.shared.util.SpjEvaluate;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Izvod implements Serializable, SpjDomainModel
{

    private static final long serialVersionUID = 1L;

    private Long id;
    private Date datum;
    private Long iznos;
    private String opis;
    private Konto konto;
    private Integer brojIzvoda;
    private Boolean zakljucen;

    private Izvod(Long id, Date datum, Long iznos, String opis, Konto konto, Integer brojIzvoda, Boolean zakljucen)
    {
        this.id = id;
        this.datum = datum;
        this.iznos = iznos;
        this.opis = opis;
        this.konto = konto;
        this.brojIzvoda = brojIzvoda;
        this.zakljucen = zakljucen;
    }

    @Override
    public String getStringIdentifier()
    {
        return getId() + getOpis();
    }

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        SpjAssert.isNotNull(id, "id");

        this.id = id;
    }

    public Date getDatum()
    {
        return datum;
    }

    public void setDatum(Date datum)
    {
        SpjAssert.isNotNull(datum, "datum");
        this.datum = datum;
    }

    public Long getIznos()
    {
        return iznos;
    }

    public void setIznos(Long iznos)
    {
        SpjAssert.isNotNull(iznos, "iznos");
        this.iznos = iznos;
    }

    public String getOpis()
    {
        return opis;
    }

    public void setOpis(String opis)
    {
        SpjAssert.isNotNullNorEmpty(opis, "opis");
        this.opis = opis;
    }

    public Konto getKonto()
    {
        return konto;
    }

    public void setKonto(Konto konto)
    {
        SpjAssert.isNotNull(konto, "konto");

        this.konto = konto;
    }

    public Integer getBrojIzvoda()
    {
        return brojIzvoda;
    }

    public void setBrojIzvoda(Integer brojIzvoda)
    {
        this.brojIzvoda = brojIzvoda;
    }

    public Boolean getZakljucen()
    {
        return zakljucen;
    }

    public void setZakljucen(Boolean zakljucen)
    {
        this.zakljucen = zakljucen;
    }

    @Override
    public boolean equals(Object otherAsObject)
    {

        if (this == otherAsObject)
        {
            return true;
        }
        if (otherAsObject == null)
        {
            return false;
        }

        if (getClass() != otherAsObject.getClass())
        {
            return false;
        }

        Izvod other = (Izvod) otherAsObject;

        return Objects.equal(this.id, other.id) &&
                Objects.equal(this.datum, other.datum) &&
                Objects.equal(this.iznos, other.iznos) &&
                Objects.equal(this.opis, other.opis) &&
                Objects.equal(this.konto, other.konto) &&
                Objects.equal(this.brojIzvoda, other.brojIzvoda) &&
                Objects.equal(this.zakljucen, this.zakljucen);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(this.id, this.datum, this.iznos, this.opis, this.konto, this.brojIzvoda, this.zakljucen);
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public static class Builder implements SpjDomainModel.Builder
    {
        private Long id;
        private Date datum;
        private Long iznos;
        private String opis;
        private Konto konto;
        private Integer brojIzvoda;
        private Boolean zakljucen;

        public Builder datum(Date datum)
        {
            SpjAssert.isNotNull(datum, "datum");

            this.datum = datum;

            return this;
        }


        public Builder id(String id)
        {
            SpjAssert.isNotNull(id, "id");

            if (!id.isEmpty())
            {
                this.id = Long.valueOf(id);
            }

            return this;
        }

        public Builder iznos(Long iznos)
        {
            SpjAssert.isNotNull(iznos, "iznos");

            this.iznos = iznos;

            return this;
        }

        public Builder opis(String opis)
        {
            SpjAssert.isNotNull(opis, "opis");

            this.opis = opis;

            return this;
        }

        public Builder konto(Konto konto)
        {
            SpjAssert.isNotNull(konto, "konto");

            this.konto = konto;

            return this;
        }

        public Builder brojIzvoda(Integer brojIzvoda)
        {
            SpjAssert.isNotNull(brojIzvoda, "brojIzvoda");

            this.brojIzvoda = brojIzvoda;

            return this;
        }

        public Builder zakljucen(Boolean zakljucen)
        {
            SpjAssert.isNotNull(zakljucen, "zakljucen");

            this.zakljucen = zakljucen;

            return this;
        }

        public Izvod build()
        {
            if (!validate().isEmpty())
            {
                throw new IllegalStateException("Trying to build invalid Izvod");
            }

            return new Izvod(id, datum, iznos, opis, konto, brojIzvoda, zakljucen);
        }


        public List<String> validate()
        {
            ImmutableList.Builder<String> resultBuilder = ImmutableList.builder();

            return resultBuilder.build();
        }


        public Izvod defaultValue()
        {
            return new Izvod(0L, new Date(), 0L, "", Konto.builder().defaultValue(), 0, false);
        }
    }


}
