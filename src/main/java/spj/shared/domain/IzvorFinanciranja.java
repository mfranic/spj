package spj.shared.domain;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import spj.shared.util.SpjAssert;
import spj.shared.util.SpjEvaluate;

import java.util.List;

public class IzvorFinanciranja extends SpjCodebookModel
{
    private IzvorFinanciranja(Long id, String naziv)
    {
        super(id, naziv);
    }

    @Override
    public boolean equals(Object otherAsObject)
    {

        if (this == otherAsObject)
        {
            return true;
        }
        if (otherAsObject == null)
        {
            return false;
        }

        if (getClass() != otherAsObject.getClass())
        {
            return false;
        }

        IzvorFinanciranja other = (IzvorFinanciranja) otherAsObject;

        return Objects.equal(this.id, other.id) &&
                Objects.equal(this.naziv, other.naziv);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(this.id, this.naziv);
    }

    public static IzvorFinanciranja.Builder builder()
    {
        return new IzvorFinanciranja.Builder();
    }

    public static IzvorFinanciranja.Builder builder(IzvorFinanciranja izvorFinanciranja)
    {
        SpjAssert.isNotNull(izvorFinanciranja, "izvorFinanciranja");

        IzvorFinanciranja.Builder builder = new IzvorFinanciranja.Builder();
        builder.id(izvorFinanciranja.getIdString());
        builder.naziv(izvorFinanciranja.getNaziv());

        return builder;
    }

    public static class Builder extends SpjCodebookModel.Builder
    {
        @Override
        public IzvorFinanciranja.Builder id(String id)
        {
            SpjAssert.isNotNull(id, "id");

            if (!id.isEmpty())
            {
                this.id = Long.valueOf(id);
            }

            return this;
        }

        @Override
        public IzvorFinanciranja.Builder naziv(String naziv)
        {
            SpjAssert.isNotNull(naziv, "naziv");

            this.naziv = naziv;

            return this;
        }

        public IzvorFinanciranja build()
        {
            if (!validate().isEmpty())
            {
                throw new IllegalStateException("Trying to build invalid IzvorFinanciranja");
            }

            return new IzvorFinanciranja(id, naziv);
        }

        @Override
        public List<String> validate()
        {
            ImmutableList.Builder<String> resultBuilder = ImmutableList.builder();

            if(!SpjEvaluate.isNotNullNorEmpty(this.naziv))
                resultBuilder.add("Naziv izvora financiranja ne smije biti prazan");

            if(!SpjEvaluate.isGreaterThan(this.id, 10L) || !SpjEvaluate.isLesserThan(this.id, 100L))
                resultBuilder.add("Sifra izvora financiranja mora biti dvoznamenkasti broj veci od 10!");

            return resultBuilder.build();
        }

        @Override
        public IzvorFinanciranja defaultValue()
        {
            return new IzvorFinanciranja(0L, "");
        }
    }
}
