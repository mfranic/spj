package spj.shared.domain;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import spj.shared.util.SpjAssert;
import spj.shared.util.SpjEvaluate;

import java.util.List;

public class OrganizacijskaJedinica extends SpjCodebookModel
{
    private final KorisnikProracuna korisnikProracuna;

    private OrganizacijskaJedinica(Long id, String naziv, KorisnikProracuna korisnikProracuna)
    {
        super(id, naziv);
        this.korisnikProracuna = korisnikProracuna;
    }

    public KorisnikProracuna getKorisnikProracuna()
    {
        return korisnikProracuna;
    }

    @Override
    public boolean equals(Object otherAsObject)
    {

        if (this == otherAsObject)
        {
            return true;
        }
        if (otherAsObject == null)
        {
            return false;
        }

        if (getClass() != otherAsObject.getClass())
        {
            return false;
        }

        OrganizacijskaJedinica other = (OrganizacijskaJedinica) otherAsObject;

        return Objects.equal(this.id, other.id) &&
                Objects.equal(this.naziv, other.naziv) &&
                Objects.equal(this.korisnikProracuna, other.korisnikProracuna);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(this.id, this.naziv, this.korisnikProracuna);
    }

    public static OrganizacijskaJedinica.Builder builder()
    {
        return new OrganizacijskaJedinica.Builder();
    }

    public static OrganizacijskaJedinica.Builder builder(OrganizacijskaJedinica organizacijskaJedinica)
    {
        OrganizacijskaJedinica.Builder builder = new OrganizacijskaJedinica.Builder();
        builder.id(organizacijskaJedinica.getIdString());
        builder.naziv(organizacijskaJedinica.getNaziv());
        builder.korisnikProracuna(organizacijskaJedinica.getKorisnikProracuna());

        return builder;
    }

    public static class Builder extends SpjCodebookModel.Builder
    {
        private KorisnikProracuna korisnikProracuna;

        @Override
        public OrganizacijskaJedinica.Builder id(String id)
        {
            SpjAssert.isNotNull(id, "id");

            if (!id.isEmpty())
            {
                this.id = Long.valueOf(id);
            }

            return this;
        }

        @Override
        public OrganizacijskaJedinica.Builder naziv(String naziv)
        {
            SpjAssert.isNotNull(naziv, "naziv");

            this.naziv = naziv;

            return this;
        }

        public OrganizacijskaJedinica.Builder korisnikProracuna(KorisnikProracuna korisnikProracuna)
        {
            SpjAssert.isNotNull(korisnikProracuna, "korisnikProracuna");

            this.korisnikProracuna = korisnikProracuna;

            return this;
        }

        public OrganizacijskaJedinica build()
        {
            if (!validate().isEmpty())
            {
                throw new IllegalStateException("Trying to build invalid OrganizacijskaJedinica");
            }

            return new OrganizacijskaJedinica(id, naziv, korisnikProracuna);
        }

        @Override
        public List<String> validate()
        {
            ImmutableList.Builder<String> resultBuilder = ImmutableList.builder();

            if(!SpjEvaluate.isNotNullNorEmpty(this.naziv))
                resultBuilder.add("Naziv organizacijske jedinice ne smije biti prazan!");

            if(!SpjEvaluate.isNotNull(this.korisnikProracuna))
                resultBuilder.add("Organizacijska jedinica mora pripadati korisniku proračuna!");

            if(!SpjEvaluate.isGreaterThan(this.id, 0L))
                resultBuilder.add("Sifra organizacijske jedinice mora biti pozitivan broj!");

            return resultBuilder.build();
        }

        @Override
        public OrganizacijskaJedinica defaultValue()
        {
            return new OrganizacijskaJedinica(0L, "", KorisnikProracuna.builder().defaultValue());
        }
    }
}
