package spj.shared.domain;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import spj.shared.util.SpjAssert;
import spj.shared.util.SpjEvaluate;

import java.util.List;

public class KorisnikProracuna extends SpjCodebookModel
{
    private final Glava glava;

    private KorisnikProracuna(Long id, String naziv, Glava glava)
    {
        super(id, naziv);
        this.glava = glava;
    }

    public Glava getGlava()
    {
        return this.glava;
    }

    @Override
    public boolean equals(Object otherAsObject)
    {

        if (this == otherAsObject)
        {
            return true;
        }
        if (otherAsObject == null)
        {
            return false;
        }

        if (getClass() != otherAsObject.getClass())
        {
            return false;
        }

        KorisnikProracuna other = (KorisnikProracuna) otherAsObject;

        return Objects.equal(this.id, other.id) &&
                Objects.equal(this.naziv, other.naziv) &&
                Objects.equal(this.glava, other.glava);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(this.id, this.naziv, this.glava);
    }

    public static KorisnikProracuna.Builder builder()
    {
        return new KorisnikProracuna.Builder();
    }

    public static KorisnikProracuna.Builder builder(KorisnikProracuna korisnikProracuna)
    {
        KorisnikProracuna.Builder builder =  new KorisnikProracuna.Builder();
        builder.id(korisnikProracuna.getIdString());
        builder.naziv(korisnikProracuna.getNaziv());
        builder.glava(korisnikProracuna.getGlava());

        return builder;
    }

    public static class Builder extends SpjCodebookModel.Builder
    {
        private Glava glava;

        @Override
        public KorisnikProracuna.Builder id(String id)
        {
            SpjAssert.isNotNull(id, "id");

            if (!id.isEmpty())
            {
                this.id = Long.valueOf(id);
            }

            return this;
        }

        @Override
        public KorisnikProracuna.Builder naziv(String naziv)
        {
            SpjAssert.isNotNull(naziv, "naziv");

            this.naziv = naziv;

            return this;
        }

        public KorisnikProracuna.Builder glava(Glava glava)
        {
            SpjAssert.isNotNull(glava, "glava");

            this.glava = glava;

            return this;
        }

        public KorisnikProracuna build()
        {
            if (!validate().isEmpty())
            {
                throw new IllegalStateException("Trying to build invalid KorisnikProracuna");
            }

            return new KorisnikProracuna(id, naziv, glava);
        }

        @Override
        public List<String> validate()
        {
            ImmutableList.Builder<String> resultBuilder = ImmutableList.builder();

            if(!SpjEvaluate.isNotNullNorEmpty(this.naziv))
                resultBuilder.add("Naziv korisnika proračuna ne smije biti prazan!");

            if(!SpjEvaluate.isNotNull(this.glava))
                resultBuilder.add("Korisnik proračuna mora pripadati glavi!");

            if(!SpjEvaluate.isGreaterThan(this.id, 0L) || !SpjEvaluate.isLesserThan(this.id, 100000L))
                resultBuilder.add("Sifra korisnika proračuna mora biti pozitivan broj sa 2-5 znamenki!");

            return resultBuilder.build();
        }

        @Override
        public KorisnikProracuna defaultValue()
        {
            return new KorisnikProracuna(0L, "", Glava.builder().defaultValue());
        }
    }
}
