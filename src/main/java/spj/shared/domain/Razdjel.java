package spj.shared.domain;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import spj.shared.util.SpjAssert;
import spj.shared.util.SpjEvaluate;

import java.util.List;

public class Razdjel extends SpjCodebookModel
{
    private Razdjel(Long id, String naziv)
    {
        super(id, naziv);
    }

    @Override
    public boolean equals(Object otherAsObject)
    {

        if (this == otherAsObject)
        {
            return true;
        }
        if (otherAsObject == null)
        {
            return false;
        }

        if (getClass() != otherAsObject.getClass())
        {
            return false;
        }

        Razdjel other = (Razdjel) otherAsObject;

        return Objects.equal(this.id, other.id) &&
                Objects.equal(this.naziv, other.naziv);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(this.id, this.naziv);
    }

    public static Razdjel.Builder builder()
    {
        return new Razdjel.Builder();
    }

    public static Razdjel.Builder builder(Razdjel razdjel)
    {
        Razdjel.Builder builder = new Razdjel.Builder();
        builder.id(razdjel.getIdString());
        builder.naziv(razdjel.getNaziv());

        return builder;
    }

    public static class Builder extends SpjCodebookModel.Builder
    {
        @Override
        public Razdjel.Builder id(String id)
        {
            SpjAssert.isNotNull(id, "id");

            if (!id.isEmpty())
            {
                this.id = Long.valueOf(id);
            }

            return this;
        }

        @Override
        public Razdjel.Builder naziv(String naziv)
        {
            SpjAssert.isNotNull(naziv, "naziv");

            this.naziv = naziv;

            return this;
        }

        @Override
        public List<String> validate()
        {
            ImmutableList.Builder<String> resultBuilder = ImmutableList.builder();

            if(!SpjEvaluate.isNotNullNorEmpty(this.naziv))
                resultBuilder.add("Naziv razdjela ne smije biti prazan!");

            if(!SpjEvaluate.isGreaterThan(this.id, 0L) || !SpjEvaluate.isLesserThan(this.id, 1000L))
                resultBuilder.add("Sifra razdjela mora biti pozitivan troznamenkasti broj!");

            return resultBuilder.build();
        }

        @Override
        public Razdjel defaultValue()
        {
            return new Razdjel(0L, "");
        }

        @Override
        public Razdjel build()
        {
            if(!validate().isEmpty())
            {
                throw new IllegalStateException("Trying to build invalid Razdjel");
            }

            return new Razdjel(this.id, this.naziv);
        }
    }
}
