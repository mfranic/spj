package spj.shared.domain;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import spj.shared.util.SpjAssert;
import spj.shared.util.SpjEvaluate;

import java.util.List;

public class Funkcija extends SpjCodebookModel
{
    private Funkcija(Long id, String naziv)
    {
        super(id, naziv);
    }

    @Override
    public boolean equals(Object otherAsObject)
    {

        if (this == otherAsObject)
        {
            return true;
        }
        if (otherAsObject == null)
        {
            return false;
        }

        if (getClass() != otherAsObject.getClass())
        {
            return false;
        }

        Funkcija other = (Funkcija) otherAsObject;

        return Objects.equal(this.id, other.id) &&
                Objects.equal(this.naziv, other.naziv);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(this.id, this.naziv);
    }

    public static Funkcija.Builder builder()
    {
        return new Funkcija.Builder();
    }

    public static Funkcija.Builder builder(Funkcija funkcija)
    {
        Funkcija.Builder builder = new Funkcija.Builder();
        builder.id(funkcija.getIdString());
        builder.naziv(funkcija.getNaziv());

        return builder;
    }

    public static class Builder extends SpjCodebookModel.Builder
    {
        @Override
        public Funkcija.Builder id(String id)
        {
            SpjAssert.isNotNull(id, "id");

            if (!id.isEmpty())
            {
                this.id = Long.valueOf(id);
            }

            return this;
        }

        @Override
        public Funkcija.Builder naziv(String naziv)
        {
            SpjAssert.isNotNull(naziv, "naziv");

            this.naziv = naziv;

            return this;
        }

        @Override
        public List<String> validate()
        {
            ImmutableList.Builder<String> resultBuilder = ImmutableList.builder();

            if(!SpjEvaluate.isNotNullNorEmpty(this.naziv))
                resultBuilder.add("Naziv funkcije ne smije biti prazan!");

            if(!SpjEvaluate.isGreaterThan(this.id, 109L) || !SpjEvaluate.isLesserThan(this.id, 10000L))
                resultBuilder.add("Sifra funkcije mora biti cetveroznamenkasti broj veci od 109!");

            return resultBuilder.build();

        }

        @Override
        public Funkcija build()
        {
            if(!validate().isEmpty())
            {
                throw new IllegalStateException("Trying to build invalid Funkcija");
            }

            return new Funkcija(this.id, this.naziv);
        }

        @Override
        public Funkcija defaultValue()
        {
            return new Funkcija(0L, "");
        }
    }


}
