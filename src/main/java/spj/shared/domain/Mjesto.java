package spj.shared.domain;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import spj.shared.util.SpjAssert;
import spj.shared.util.SpjEvaluate;

import java.util.List;

public class Mjesto extends SpjCodebookModel
{
    private final String pozivniBroj;
    private final String postanskiBroj;

    protected Mjesto(Long id, String naziv, String pozivniBroj, String postanskiBroj)
    {
        super(id, naziv);
        this.pozivniBroj = pozivniBroj;
        this.postanskiBroj = postanskiBroj;
    }

    public String getPozivniBroj()
    {
        return this.pozivniBroj;
    }

    public String getPostanskiBroj()
    {
        return this.postanskiBroj;
    }

    @Override
    public boolean equals(Object otherAsObject)
    {

        if (this == otherAsObject)
        {
            return true;
        }
        if (otherAsObject == null)
        {
            return false;
        }

        if (getClass() != otherAsObject.getClass())
        {
            return false;
        }

        Mjesto other = (Mjesto) otherAsObject;

        return Objects.equal(this.id, other.id) &&
                Objects.equal(this.naziv, other.naziv) &&
                Objects.equal(this.pozivniBroj, other.pozivniBroj) &&
                Objects.equal(this.postanskiBroj, other.postanskiBroj);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(this.id, this.naziv, this.pozivniBroj, this.postanskiBroj);
    }

    public static Mjesto.Builder builder()
    {
        return new Mjesto.Builder();
    }

    public static Mjesto.Builder builder(Mjesto mjesto)
    {
        Mjesto.Builder builder = new Mjesto.Builder();
        builder.id(mjesto.getIdString());
        builder.naziv(mjesto.getNaziv());
        builder.pozivniBroj(mjesto.getPozivniBroj());
        builder.postanskiBroj(mjesto.getPostanskiBroj());

        return builder;
    }

    public static class Builder extends SpjCodebookModel.Builder
    {
        private String pozivniBroj;
        private String postanskiBroj;

        @Override
        public Mjesto.Builder id(String id)
        {
            SpjAssert.isNotNull(id, "id");

            if (!id.isEmpty())
            {
                this.id = Long.valueOf(id);
            }

            return this;
        }

        @Override
        public Mjesto.Builder naziv(String naziv)
        {
            SpjAssert.isNotNull(naziv, "naziv");

            this.naziv = naziv;

            return this;
        }

        public Mjesto.Builder pozivniBroj(String pozivniBroj)
        {
            SpjAssert.isNotNull(pozivniBroj, "pozivniBroj");

            this.pozivniBroj = pozivniBroj;

            return this;
        }

        public Mjesto.Builder postanskiBroj(String postanskiBroj)
        {
            SpjAssert.isNotNull(postanskiBroj, "postanskiBroj");

            this.postanskiBroj = postanskiBroj;

            return this;
        }

        @Override
        public List<String> validate()
        {
            ImmutableList.Builder<String> resultBuilder = ImmutableList.builder();

            if(!SpjEvaluate.isNotNullNorEmpty(this.naziv))
                resultBuilder.add("Naziv mjesta ne smije biti prazan!");

            if(!SpjEvaluate.isGreaterThan(this.id, 0L))
                resultBuilder.add("Sifra mjesta mora biti pozitivan broj!");

            return resultBuilder.build();
        }

        @Override
        public Mjesto defaultValue()
        {
            return new Mjesto(0L, "", "", "");
        }

        @Override
        public Mjesto build()
        {
            if(!validate().isEmpty())
            {
                throw new IllegalStateException("Trying to build invalid Mjesto");
            }

            return new Mjesto(this.id, this.naziv, this.pozivniBroj, this.postanskiBroj);
        }
    }
}
