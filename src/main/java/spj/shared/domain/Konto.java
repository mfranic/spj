package spj.shared.domain;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import spj.shared.util.SpjAssert;
import spj.shared.util.SpjEvaluate;

import java.io.Serializable;
import java.util.List;

public class Konto implements Serializable, SpjDomainModel
{
    private static final long serialVersionUID = 1L;

    private Long id;
    private String naziv;
    private Boolean imaSaldokonto;
    private Boolean koristiSe;
    private Boolean obveza;
    private Boolean potrazivanje;

    private Konto(Long id, String naziv, Boolean imaSaldokonto, Boolean koristiSe, Boolean obveza, Boolean potrazivanje)
    {
        this.id = id;
        this.naziv = naziv;
        this.imaSaldokonto = imaSaldokonto;
        this.koristiSe = koristiSe;
        this.obveza = obveza;
        this.potrazivanje = potrazivanje;
    }

    @Override
    public Long getId()
    {
        return id;
    }

    @Override
    public String getStringIdentifier()
    {
        return naziv;
    }

    public void setId(Long id)
    {
        SpjAssert.isNotNull(id, "id");

        this.id = id;
    }

    public String getIdentifier()
    {
        return getNaziv();
    }

    public String getNaziv()
    {
        return naziv;
    }

    public void setNaziv(String naziv)
    {
        SpjAssert.isNotNullNorEmpty(naziv, "naziv");

        this.naziv = naziv;
    }

    public Boolean getImaSaldokonto()
    {
        return imaSaldokonto;
    }

    public void setImaSaldokonto(Boolean imaSaldokonto)
    {
        SpjAssert.isNotNull(imaSaldokonto, "imaSaldokonto");

        this.imaSaldokonto = imaSaldokonto;
    }

    public Boolean getKoristiSe()
    {
        return koristiSe;
    }

    public void setKoristiSe(Boolean koristiSe)
    {
        SpjAssert.isNotNull(koristiSe, "koristiSe");

        this.koristiSe = koristiSe;
    }

    public Boolean getObveza()
    {
        return obveza;
    }

    public void setObveza(Boolean obveza)
    {
        SpjAssert.isNotNull(obveza, "obveza");
        this.obveza = obveza;
    }

    public Boolean getPotrazivanje()
    {
        return potrazivanje;
    }

    public void setPotrazivanje(Boolean potrazivanje)
    {
        SpjAssert.isNotNull(potrazivanje, "potrazivanje");

        this.potrazivanje = potrazivanje;
    }

    @Override
    public boolean equals(Object otherAsObject)
    {

        if (this == otherAsObject)
        {
            return true;
        }
        if (otherAsObject == null)
        {
            return false;
        }

        if (getClass() != otherAsObject.getClass())
        {
            return false;
        }

        Konto other = (Konto) otherAsObject;

        return Objects.equal(this.id, other.id) &&
                Objects.equal(this.naziv, other.naziv) &&
                Objects.equal(this.imaSaldokonto, other.imaSaldokonto) &&
                Objects.equal(this.koristiSe, other.koristiSe) &&
                Objects.equal(this.obveza, other.obveza) &&
                Objects.equal(this.potrazivanje, this.potrazivanje);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(this.id, this.naziv, this.imaSaldokonto, this.koristiSe, this.obveza, this.potrazivanje);
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public static class Builder implements SpjDomainModel.Builder
    {
        private Long id;
        private String naziv;
        private Boolean imaSaldokonto;
        private Boolean koristiSe;
        private Boolean obveza;
        private Boolean potrazivanje;

        public Builder naziv(String naziv)
        {
            SpjAssert.isNotNull(naziv, "naziv");

            this.naziv = naziv;

            return this;
        }

        public Builder id(String id)
        {
            SpjAssert.isNotNull(id, "id");

            if (!id.isEmpty())
            {
                this.id = Long.valueOf(id);
            }

            return this;
        }
        public Builder imaSaldokonto(Boolean imaSaldokonto)
        {
            SpjAssert.isNotNull(imaSaldokonto, "imaSaldokonto");

            this.imaSaldokonto = imaSaldokonto;

            return this;
        }

        public Builder koristiSe(Boolean koristiSe)
        {
            SpjAssert.isNotNull(koristiSe, "koristiSe");

            this.koristiSe = koristiSe;

            return this;
        }

        public Builder obveza(Boolean obveza)
        {
            SpjAssert.isNotNull(obveza, "obveza");

            this.obveza = obveza;

            return this;
        }

        public Builder potrazivanje(Boolean potrazivanje)
        {
            SpjAssert.isNotNull(potrazivanje, "potrazivanje");

            this.potrazivanje = potrazivanje;

            return this;
        }

        public Konto build()
        {
            if (!validate().isEmpty())
            {
                throw new IllegalStateException("Trying to build invalid Konto");
            }

            return new Konto(id, naziv, imaSaldokonto, koristiSe, obveza, potrazivanje);
        }

        public List<String> validate()
        {
            ImmutableList.Builder<String> resultBuilder = ImmutableList.builder();

            if(!SpjEvaluate.isNotNullNorEmpty(this.naziv))
                resultBuilder.add("Naziv konta prazan");

            //TODO finish validate func

            return resultBuilder.build();        }

        public Konto defaultValue()
        {
            return new Konto(0L, "", false, false, false, false);
        }
    }
}

