package spj.shared.domain;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import spj.shared.util.SpjAssert;
import spj.shared.util.SpjEvaluate;

import java.util.List;

public class MaticniPodaci extends SpjCodebookModel
{
    private final String telefon;
    private final String odgovornaOsoba;
    private final String fax;
    private final String ziroRacun;
    private final String oib;
    private final String adresa;
    private final Mjesto mjesto;

    private MaticniPodaci(
            Long id,
            String naziv,
            String telefon,
            String odgovornaOsoba,
            String fax,
            String ziroRacun,
            String oib,
            String adresa,
            Mjesto mjesto)
    {
        super(id, naziv);
        this.telefon = telefon;
        this.odgovornaOsoba = odgovornaOsoba;
        this.fax = fax;
        this.ziroRacun = ziroRacun;
        this.oib = oib;
        this.adresa = adresa;
        this.mjesto = mjesto;
    }

    public String getTelefon()
    {
        return telefon;
    }

    public String getOdgovornaOsoba()
    {
        return odgovornaOsoba;
    }

    public String getFax()
    {
        return fax;
    }

    public String getZiroRacun()
    {
        return ziroRacun;
    }

    public String getOib()
    {
        return oib;
    }

    public String getAdresa()
    {
        return adresa;
    }

    public Mjesto getMjesto()
    {
        return mjesto;
    }

    @Override
    public boolean equals(Object otherAsObject)
    {

        if (this == otherAsObject)
        {
            return true;
        }
        if (otherAsObject == null)
        {
            return false;
        }

        if (getClass() != otherAsObject.getClass())
        {
            return false;
        }

        MaticniPodaci other = (MaticniPodaci) otherAsObject;

        return Objects.equal(this.id, other.id) &&
                Objects.equal(this.naziv, other.naziv) &&
                Objects.equal(this.telefon, other.telefon) &&
                Objects.equal(this.odgovornaOsoba, other.odgovornaOsoba) &&
                Objects.equal(this.fax, other.fax) &&
                Objects.equal(this.ziroRacun, other.ziroRacun) &&
                Objects.equal(this.oib, other.oib) &&
                Objects.equal(this.adresa, other.adresa) &&
                Objects.equal(this.mjesto, other.mjesto);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(this.id, this.naziv, this.telefon, this.odgovornaOsoba, this.fax, this.ziroRacun, this.oib, this.adresa, this.mjesto);
    }

    public static MaticniPodaci.Builder builder()
    {
        return new MaticniPodaci.Builder();
    }

    public static MaticniPodaci.Builder builder(MaticniPodaci maticniPodaci)
    {
        MaticniPodaci.Builder builder = new MaticniPodaci.Builder();
        builder.id(maticniPodaci.getIdString());
        builder.naziv(maticniPodaci.getNaziv());
        builder.telefon(maticniPodaci.getTelefon());
        builder.odgovornaOsoba(maticniPodaci.getOdgovornaOsoba());
        builder.fax(maticniPodaci.getFax());
        builder.ziroRacun(maticniPodaci.getZiroRacun());
        builder.oib(maticniPodaci.getOib());
        builder.adresa(maticniPodaci.getAdresa());
        builder.mjesto(maticniPodaci.getMjesto());

        return builder;
    }

    public static class Builder extends SpjCodebookModel.Builder
    {
        private String telefon;
        private String odgovornaOsoba;
        private String fax;
        private String ziroRacun;
        private String oib;
        private String adresa;
        private Mjesto mjesto;

        @Override
        public MaticniPodaci.Builder id(String id)
        {
            SpjAssert.isNotNull(id, "id");

            if (!id.isEmpty())
            {
                this.id = Long.valueOf(id);
            }

            return this;
        }

        @Override
        public MaticniPodaci.Builder naziv(String naziv)
        {
            SpjAssert.isNotNull(naziv, "naziv");

            this.naziv = naziv;

            return this;
        }

        public MaticniPodaci.Builder telefon(String telefon)
        {
            SpjAssert.isNotNull(telefon, "telefon");

            this.telefon = telefon;

            return this;
        }

        public MaticniPodaci.Builder odgovornaOsoba(String odgovornaOsoba)
        {
            SpjAssert.isNotNull(odgovornaOsoba, "odgovornaOsoba");

            this.odgovornaOsoba = odgovornaOsoba;

            return this;
        }

        public MaticniPodaci.Builder fax(String fax)
        {
            SpjAssert.isNotNull(fax, "fax");

            this.fax = fax;

            return this;
        }

        public MaticniPodaci.Builder ziroRacun(String ziroRacun)
        {
            SpjAssert.isNotNull(ziroRacun, "ziroRacun");

            this.ziroRacun = ziroRacun;

            return this;
        }

        public MaticniPodaci.Builder oib(String oib)
        {
            SpjAssert.isNotNull(oib, "oib");

            this.oib = oib;

            return this;
        }

        public MaticniPodaci.Builder adresa(String adresa)
        {
            SpjAssert.isNotNull(adresa, "adresa");

            this.adresa = adresa;

            return this;
        }

        public MaticniPodaci.Builder mjesto(Mjesto mjesto)
        {
            SpjAssert.isNotNull(mjesto, "mjesto");

            this.mjesto = mjesto;

            return this;
        }

        @Override
        public List<String> validate()
        {
            ImmutableList.Builder<String> resultBuilder = ImmutableList.builder();

            if(!SpjEvaluate.isNotNullNorEmpty(this.naziv))
                resultBuilder.add("Naziv maticnih podataka ne smije biti prazan!");

            if(!SpjEvaluate.isNotNull(this.mjesto))
                resultBuilder.add("Maticni podaci moraju imati mjesto!");

            if(!SpjEvaluate.isGreaterThan(this.id, 0L))
                resultBuilder.add("Sifra maticnih podataka mora biti pozitivan broj!");

            return resultBuilder.build();
        }

        @Override
        public MaticniPodaci defaultValue()
        {
            return new MaticniPodaci(0L, "", "", "", "", "", "", "", Mjesto.builder().defaultValue());
        }

        @Override
        public MaticniPodaci build()
        {
            if(!validate().isEmpty())
            {
                throw new IllegalStateException("Trying to build invalid MaticniPodaci");
            }

            return new MaticniPodaci(this.id, this.naziv, this.telefon, this.odgovornaOsoba, this.fax, this.ziroRacun, this.oib, this.adresa, this.mjesto);
        }
    }
}
