package spj.shared.domain;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import spj.shared.util.SpjAssert;
import spj.shared.util.SpjEvaluate;
import spj.shared.util.SpjStringIdTool;

import java.util.List;

public class Aktivnost extends SpjCodebookModel
{
    private final String zakonskaOsnova;
    private final String opis;
    private final String opciCilj;
    private final Program program;
    private final Funkcija funkcija;
    private final Mjesto mjesto;
    private final IzvorFinanciranja izvorFinanciranja;
    private final String pokazateljUspijeha;

    private Aktivnost(
            Long id,
            String naziv,
            String zakonskaOsnova,
            String opis,
            String opciCilj,
            Program program,
            Funkcija funkcija,
            Mjesto mjesto,
            IzvorFinanciranja izvorFinanciranja,
            String pokazateljUspijeha
    )
    {
        super(id, naziv);
        this.zakonskaOsnova = zakonskaOsnova;
        this.opis = opis;
        this.opciCilj = opciCilj;
        this.program = program;
        this.funkcija = funkcija;
        this.mjesto = mjesto;
        this.izvorFinanciranja = izvorFinanciranja;
        this.pokazateljUspijeha = pokazateljUspijeha;
    }

    @Override
    public String getIdString()
    {
        return SpjStringIdTool.stringWithBeginingCharacterFromLong(this.id, 1000000);
    }

    public String getZakonskaOsnova()
    {
        return zakonskaOsnova;
    }

    public String getOpis()
    {
        return opis;
    }

    public String getOpciCilj()
    {
        return opciCilj;
    }

    public Program getProgram()
    {
        return program;
    }

    public Funkcija getFunkcija()
    {
        return funkcija;
    }

    public Mjesto getMjesto()
    {
        return mjesto;
    }

    public IzvorFinanciranja getIzvorFinanciranja()
    {
        return izvorFinanciranja;
    }

    public String getPokazateljUspijeha()
    {
        return pokazateljUspijeha;
    }

    @Override
    public boolean equals(Object otherAsObject)
    {

        if (this == otherAsObject)
        {
            return true;
        }
        if (otherAsObject == null)
        {
            return false;
        }

        if (getClass() != otherAsObject.getClass())
        {
            return false;
        }

        Aktivnost other = (Aktivnost) otherAsObject;

        return Objects.equal(this.id, other.id) &&
                Objects.equal(this.naziv, other.naziv) &&
                Objects.equal(this.zakonskaOsnova, other.zakonskaOsnova) &&
                Objects.equal(this.opis, other.opis) &&
                Objects.equal(this.opciCilj, other.opciCilj) &&
                Objects.equal(this.program, other.program) &&
                Objects.equal(this.funkcija, other.funkcija) &&
                Objects.equal(this.mjesto, other.mjesto) &&
                Objects.equal(this.izvorFinanciranja, other.izvorFinanciranja) &&
                Objects.equal(this.pokazateljUspijeha, other.pokazateljUspijeha);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(this.id, this.naziv, this.zakonskaOsnova, this.opis, this.opciCilj, this.program, this.funkcija, this.mjesto, this.izvorFinanciranja, this.pokazateljUspijeha);
    }

    public static Aktivnost.Builder builder()
    {
        return new Aktivnost.Builder();
    }

    public static Aktivnost.Builder builder(Aktivnost aktivnost)
    {
        Aktivnost.Builder builder = new Aktivnost.Builder();
        builder.id(aktivnost.getIdString());
        builder.naziv(aktivnost.getNaziv());
        builder.zakonskaOsnova(aktivnost.getZakonskaOsnova());
        builder.opis(aktivnost.getOpis());
        builder.opciCilj(aktivnost.getOpciCilj());
        builder.program(aktivnost.getProgram());
        builder.funkcija(aktivnost.getFunkcija());
        builder.mjesto(aktivnost.getMjesto());
        builder.izvoriFinanciranja(aktivnost.getIzvorFinanciranja());
        builder.pokazateljUspijeha(aktivnost.getPokazateljUspijeha());

        return builder;
    }

    public static class Builder extends SpjCodebookModel.Builder
    {
        private String zakonskaOsnova;
        private String opis;
        private String opciCilj;
        private Program program;
        private Funkcija funkcija;
        private Mjesto mjesto;
        private IzvorFinanciranja izvorFinanciranja;
        private String pokazateljUspijeha;

        @Override
        public Aktivnost.Builder id(String id)
        {
            SpjAssert.isNotNull(id, "id");

            if (!id.isEmpty())
            {
                this.id = SpjStringIdTool.stringWithBeginingCharacterToLong(id, 1000000);
            }

            return this;
        }

        public Aktivnost.Builder id(Long id)
        {
            SpjAssert.isNotNull(id, "id");

            this.id = id;

            return this;
        }

        @Override
        public Aktivnost.Builder naziv(String naziv)
        {
            SpjAssert.isNotNull(naziv, "naziv");

            this.naziv = naziv;

            return this;
        }

        public Aktivnost.Builder zakonskaOsnova(String zakonskaOsnova)
        {
            SpjAssert.isNotNull(zakonskaOsnova, "zakonskaOsnova");

            this.zakonskaOsnova = zakonskaOsnova;

            return this;
        }

        public Aktivnost.Builder opis(String opis)
        {
            SpjAssert.isNotNull(opis, "opis");

            this.opis = opis;

            return this;
        }

        public Aktivnost.Builder opciCilj(String opciCilj)
        {
            SpjAssert.isNotNull(opciCilj, "opciCilj");

            this.opciCilj = opciCilj;

            return this;
        }

        public Aktivnost.Builder program(Program program)
        {
            SpjAssert.isNotNull(program, "program");

            this.program = program;

            return this;
        }

        public Aktivnost.Builder funkcija(Funkcija funkcija)
        {
            SpjAssert.isNotNull(funkcija, "funkcija");

            this.funkcija = funkcija;

            return this;
        }

        public Aktivnost.Builder mjesto(Mjesto mjesto)
        {
            SpjAssert.isNotNull(mjesto, "mjesto");

            this.mjesto = mjesto;

            return this;
        }

        public Aktivnost.Builder izvoriFinanciranja(IzvorFinanciranja izvorFinanciranja)
        {
            SpjAssert.isNotNull(izvorFinanciranja, "izvorFinanciranja");

            this.izvorFinanciranja = izvorFinanciranja;

            return this;
        }

        public Aktivnost.Builder pokazateljUspijeha(String pokazateljUspijeha)
        {
            SpjAssert.isNotNull(pokazateljUspijeha, "pokazateljUspijeha");

            this.pokazateljUspijeha = pokazateljUspijeha;

            return this;
        }

        @Override
        public List<String> validate()
        {
            ImmutableList.Builder<String> resultBuilder = ImmutableList.builder();

            if (!SpjEvaluate.isNotNullNorEmpty(this.naziv))
            {
                resultBuilder.add("Naziv aktivnosti ne smije biti prazan!");
            }

            if (!SpjEvaluate.isNotNull(this.program))
            {
                resultBuilder.add("Aktivnost mora pripadati programu!");
            }

            if (!SpjEvaluate.isNotNull(this.funkcija))
            {
                resultBuilder.add("Aktivnost mora pripadati funkciji!");
            }

            if (!SpjEvaluate.isNotNull(this.mjesto))
            {
                resultBuilder.add("Aktivnost mora imati lokaciju!");
            }

            if (!SpjEvaluate.isNotNull(this.izvorFinanciranja))
            {
                resultBuilder.add("Aktivnost mora imati izvor financiranja!");
            }


            if (
                    !(SpjEvaluate.isGreaterThan(this.id, 65100000L) && SpjEvaluate.isLesserThan(this.id, 651000000L)) &&
                            !(SpjEvaluate.isGreaterThan(this.id, 75100000L) && SpjEvaluate.isLesserThan(this.id, 751000000L)) &&
                            !(SpjEvaluate.isGreaterThan(this.id, 84100000L) && SpjEvaluate.isLesserThan(this.id, 841000000L))
                    )
            {
                resultBuilder.add("Sifra aktivnosti mora biti A, K ili T te šesteroznamenkasti broj veci od 100000!");
            }

            return resultBuilder.build();
        }

        @Override
        public Aktivnost defaultValue()
        {
            return new Aktivnost(
                    0L, "", "", "", "",
                    Program.builder().defaultValue(),
                    Funkcija.builder().defaultValue(),
                    Mjesto.builder().defaultValue(),
                    IzvorFinanciranja.builder().defaultValue(),
                    ""
            );
        }

        @Override
        public Aktivnost build()
        {
            if (!validate().isEmpty())
            {
                throw new IllegalStateException("Trying to build invalid Aktivnost");
            }

            return new Aktivnost(
                    this.id,
                    this.naziv,
                    this.zakonskaOsnova,
                    this.opis,
                    this.opciCilj,
                    this.program,
                    this.funkcija,
                    this.mjesto,
                    this.izvorFinanciranja,
                    this.pokazateljUspijeha
            );
        }
    }
}
