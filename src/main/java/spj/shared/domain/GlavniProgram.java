package spj.shared.domain;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import spj.shared.util.SpjAssert;
import spj.shared.util.SpjEvaluate;
import spj.shared.util.SpjStringIdTool;
import spj.util.SpjStringTool;

import java.util.List;

public class GlavniProgram extends SpjCodebookModel
{
    private GlavniProgram(Long id, String naziv)
    {
        super(id, naziv);
    }

    @Override
    public String getIdString()
    {
        return SpjStringIdTool.stringWithBeginingCharacterFromLong(id, 100);
    }

    @Override
    public boolean equals(Object otherAsObject)
    {

        if (this == otherAsObject)
        {
            return true;
        }
        if (otherAsObject == null)
        {
            return false;
        }

        if (getClass() != otherAsObject.getClass())
        {
            return false;
        }

        GlavniProgram other = (GlavniProgram) otherAsObject;

        return Objects.equal(this.id, other.id) &&
                Objects.equal(this.naziv, other.naziv);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(this.id, this.naziv);
    }

    public static GlavniProgram.Builder builder()
    {
        return new GlavniProgram.Builder();
    }

    public static GlavniProgram.Builder builder(GlavniProgram glavniProgram)
    {
        GlavniProgram.Builder builder =  new GlavniProgram.Builder();
        builder.id(glavniProgram.getId());
        builder.naziv(glavniProgram.getNaziv());

        return builder;
    }

    public static class Builder extends SpjCodebookModel.Builder
    {
        @Override
        public GlavniProgram.Builder id(String id)
        {
            SpjAssert.isNotNull(id, "id");

            if (!id.isEmpty())
            {
                this.id = SpjStringIdTool.stringWithBeginingCharacterToLong(id, 100);
            }

            return this;
        }

        public GlavniProgram.Builder id(Long id)
        {
            SpjAssert.isNotNull(id, "id");

            this.id = id;

            return this;
        }

        @Override
        public GlavniProgram.Builder naziv(String naziv)
        {
            SpjAssert.isNotNull(naziv, "naziv");

            this.naziv = naziv;

            return this;
        }

        public GlavniProgram build()
        {
            if (!validate().isEmpty())
            {
                throw new IllegalStateException("Trying to build invalid GlavniProgram");
            }

            return new GlavniProgram(id, naziv);
        }

        @Override
        public List<String> validate()
        {

            ImmutableList.Builder<String> resultBuilder = ImmutableList.builder();

            if (!SpjEvaluate.isNotNullNorEmpty(this.naziv))
            {
                resultBuilder.add("Naziv glavnog programa ne smije biti prazan");
            }

            if (!SpjEvaluate.isGreaterThan(this.id, 6500L)
                    || !SpjEvaluate.isLesserThan(this.id, 12300L)
                    || (!SpjEvaluate.isLesserThan(this.id, 9100L) && !SpjEvaluate.isGreaterThan(this.id, 9700L))
                    )
            {
                resultBuilder.add("Sifra glavnog program mora biti slovo i pozitivan dvoznamenkasti broj!");
            }

            return resultBuilder.build();

        }

        @Override
        public GlavniProgram defaultValue()
        {
            return new GlavniProgram(0L, "");
        }
    }
}
