package spj.shared.domain;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import spj.shared.util.SpjAssert;
import spj.shared.util.SpjEvaluate;

import java.util.List;

public class Partner extends SpjCodebookModel
{
    private final String telefon;
    private final String odgovornaOsoba;
    private final String fax;
    private final String ziroRacun;
    private final String oib;
    private final String email;
    private final String adresa;
    private final Mjesto mjesto;

    private Partner(
            Long id,
            String naziv,
            String telefon,
            String odgovornaOsoba,
            String fax,
            String ziroRacun,
            String oib,
            String email,
            String adresa,
            Mjesto mjesto)
    {
        super(id, naziv);
        this.telefon = telefon;
        this.odgovornaOsoba = odgovornaOsoba;
        this.fax = fax;
        this.ziroRacun = ziroRacun;
        this.oib = oib;
        this.email = email;
        this.adresa = adresa;
        this.mjesto = mjesto;
    }

    public String getTelefon()
    {
        return telefon;
    }

    public String getOdgovornaOsoba()
    {
        return odgovornaOsoba;
    }

    public String getFax()
    {
        return fax;
    }

    public String getZiroRacun()
    {
        return ziroRacun;
    }

    public String getOib()
    {
        return oib;
    }

    public String getEmail()
    {
        return email;
    }

    public String getAdresa()
    {
        return adresa;
    }

    public Mjesto getMjesto()
    {
        return mjesto;
    }

    @Override
    public boolean equals(Object otherAsObject)
    {

        if (this == otherAsObject)
        {
            return true;
        }
        if (otherAsObject == null)
        {
            return false;
        }

        if (getClass() != otherAsObject.getClass())
        {
            return false;
        }

        Partner other = (Partner) otherAsObject;

        return Objects.equal(this.id, other.id) &&
                Objects.equal(this.naziv, other.naziv) &&
                Objects.equal(this.telefon, other.telefon) &&
                Objects.equal(this.odgovornaOsoba, other.odgovornaOsoba) &&
                Objects.equal(this.fax, other.fax) &&
                Objects.equal(this.ziroRacun, other.ziroRacun) &&
                Objects.equal(this.oib, other.oib) &&
                Objects.equal(this.email, other.email) &&
                Objects.equal(this.adresa, other.adresa) &&
                Objects.equal(this.mjesto, other.mjesto);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(this.id, this.naziv, this.telefon, this.odgovornaOsoba, this.fax, this.ziroRacun, this.oib, this.email, this.adresa, this.mjesto);
    }

    public static Partner.Builder builder()
    {
        return new Partner.Builder();
    }

    public static Partner.Builder builder(Partner partner)
    {
        Partner.Builder builder = new Partner.Builder();
        builder.id(partner.getIdString());
        builder.naziv(partner.getNaziv());
        builder.telefon(partner.getTelefon());
        builder.odgovornaOsoba(partner.getOdgovornaOsoba());
        builder.fax(partner.getFax());
        builder.ziroRacun(partner.getZiroRacun());
        builder.oib(partner.getOib());
        builder.email(partner.getEmail());
        builder.adresa(partner.getAdresa());
        builder.mjesto(partner.getMjesto());

        return builder;
    }

    public static class Builder extends SpjCodebookModel.Builder
    {
        private String telefon;
        private String odgovornaOsoba;
        private String fax;
        private String ziroRacun;
        private String oib;
        private String email;
        private String adresa;
        private Mjesto mjesto;

        @Override
        public Partner.Builder id(String id)
        {
            SpjAssert.isNotNull(id, "id");

            if (!id.isEmpty())
            {
                this.id = Long.valueOf(id);
            }

            return this;
        }

        @Override
        public Partner.Builder naziv(String naziv)
        {
            SpjAssert.isNotNull(naziv, "naziv");

            this.naziv = naziv;

            return this;
        }

        public Partner.Builder telefon(String telefon)
        {
            SpjAssert.isNotNull(telefon, "telefon");

            this.telefon = telefon;

            return this;
        }

        public Partner.Builder odgovornaOsoba(String odgovornaOsoba)
        {
            SpjAssert.isNotNull(odgovornaOsoba, "odgovornaOsoba");

            this.odgovornaOsoba = odgovornaOsoba;

            return this;
        }

        public Partner.Builder fax(String fax)
        {
            SpjAssert.isNotNull(fax, "fax");

            this.fax = fax;

            return this;
        }

        public Partner.Builder ziroRacun(String ziroRacun)
        {
            SpjAssert.isNotNull(ziroRacun, "ziroRacun");

            this.ziroRacun = ziroRacun;

            return this;
        }

        public Partner.Builder oib(String oib)
        {
            SpjAssert.isNotNull(oib, "oib");

            this.oib = oib;

            return this;
        }

        public Partner.Builder email(String email)
        {
            SpjAssert.isNotNull(email, "email");

            this.email = email;

            return this;
        }

        public Partner.Builder adresa(String adresa)
        {
            SpjAssert.isNotNull(adresa, "adresa");

            this.adresa = adresa;

            return this;
        }

        public Partner.Builder mjesto(Mjesto mjesto)
        {
            SpjAssert.isNotNull(mjesto, "mjesto");

            this.mjesto = mjesto;

            return this;
        }

        @Override
        public List<String> validate()
        {
            ImmutableList.Builder<String> resultBuilder = ImmutableList.builder();

            if(!SpjEvaluate.isNotNullNorEmpty(this.naziv))
                resultBuilder.add("Naziv partnera ne smije biti prazan!");

            if(!SpjEvaluate.isNotNull(this.mjesto))
                resultBuilder.add("Partner mora imati mjesto!");

            if(!SpjEvaluate.isGreaterThan(this.id, 0L))
                resultBuilder.add("Sifra partnera mora biti pozitivan broj!");

            return resultBuilder.build();
        }

        @Override
        public Partner defaultValue()
        {
            return new Partner(0L, "", "", "", "", "", "", "", "", Mjesto.builder().defaultValue());
        }

        @Override
        public Partner build()
        {
            if(!validate().isEmpty())
            {
                throw new IllegalStateException("Trying to build invalid Partner");
            }

            return new Partner(this.id, this.naziv, this.telefon, this.odgovornaOsoba, this.fax, this.ziroRacun, this.oib, this.email, this.adresa, this.mjesto);
        }
    }
}
