package spj.util;

import spj.shared.util.SpjAssert;

public class SpjCastTool
{
    @SuppressWarnings("unchecked")
    public static <T> T cast(Object value)
    {
        SpjAssert.isNotNull(value, "value");

        return (T) value;
    }
}
