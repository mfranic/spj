package spj.util;

import spj.shared.util.SpjAssert;

public class SpjStringTool
{
    public static final int BEGIN_INDEX = 0;
    public static final int SECOND_INDEX = 1;

    public static String toCamelCase(String value)
    {
        SpjAssert.isNotNullNorEmpty(value, "value");

        return value.substring(BEGIN_INDEX, SECOND_INDEX).toLowerCase() + value.substring(SECOND_INDEX);
    }
}
