package spj.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;
import org.springframework.remoting.support.RemoteExporter;
import spj.shared.remoting.service.SpjRemotingService;

@Configuration
public class RemotingServiceConfiguration
{
    @Autowired
    SpjRemotingService spjRemotingService;

    @Bean(name = "/remoting/spjremotingservice")
    RemoteExporter spjRemotingService()
    {
        return createServiceExporter(spjRemotingService, SpjRemotingService.class);
    }

    private RemoteExporter createServiceExporter(Object service, Class<?> remotingClass)
    {
        HttpInvokerServiceExporter hse = new HttpInvokerServiceExporter();
        hse.setService(service);
        hse.setServiceInterface(remotingClass);
        hse.afterPropertiesSet();
        return hse;
    }
}
