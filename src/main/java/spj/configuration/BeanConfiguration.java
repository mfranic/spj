package spj.configuration;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class BeanConfiguration
{

    @Autowired
    PropertiesLoader propertiesLoader;

    @Bean
    DataSource dataSource()
    {
        BasicDataSource dataSource = new BasicDataSource();

        dataSource.setDriverClassName(propertiesLoader.getJdbcDriverName());
        dataSource.setUrl(propertiesLoader.getJdbcUrl());
        dataSource.setUsername(propertiesLoader.getJdbcUser());
        dataSource.setPassword(propertiesLoader.getJdbcPassword());

        return dataSource;
    }
}
