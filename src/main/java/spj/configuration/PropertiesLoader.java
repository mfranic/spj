package spj.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PropertiesLoader
{
    @Value("${jdbc-driver}")
    private String jdbcDriverName;
    @Value("${jdbc-url}")
    private String jdbcUrl;
    @Value("${jdbc-user}")
    private String jdbcUser;
    @Value("${jdbc-password}")
    private String jdbcPassword;

    public PropertiesLoader()
    {
    }

    public String getJdbcDriverName()
    {
        return jdbcDriverName;
    }

    public String getJdbcUrl()
    {
        return jdbcUrl;
    }

    public String getJdbcUser()
    {
        return jdbcUser;
    }

    public String getJdbcPassword()
    {
        return jdbcPassword;
    }
}
